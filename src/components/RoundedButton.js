import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const RoundedButton = ({ label, onPress,style,bgColor }) => {
  return (
    <TouchableOpacity
      style={{ alignItems: 'center', justifyContent: 'center'}}
      onPress={onPress}
    >
      <Text style={[{ fontSize: 22, color: 'white', fontWeight: 'bold',backgroundColor:bgColor },style]}>
        {label}
      </Text>
    </TouchableOpacity>
  );
};

export default RoundedButton;