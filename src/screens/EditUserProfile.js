import React, { useEffect, useReducer } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    ScrollView, Picker, Alert, AsyncStorage
} from 'react-native';
import { RadioButton } from 'react-native-paper';
import DatePicker from 'react-native-datepicker';
import DropDownPicker from 'react-native-dropdown-picker';
import FormInput from '../components/FormInput'
import { addUser, getAllData,updateUser} from '../services/Apiservices';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ActivityLoading from '../components/ActivityLoading'

const EditUserProfile = ({ route, navigation }) => {
     const [user, setUser] = React.useState(' ');
     

   const [value, setValue] = React.useState('first');
    const [isLoading, setIsLoading] = React.useState(false);

    const [values, setValues] = React.useState({
        val: [{
            domain: "",
            name: "",
            startingDate: "",
            endingDate: "",
            location: {
                city: "",
                state: "",
                abroad: false
            }

        }]


    });
    const [employment, setEmployment] = React.useState({
        data: [
            {
                comapnyName: "",                
                designation: "",
                begins: "",
                ends: "",
                period: 2,
                location: {
                    city: "",
                    state: "",
                    abroad: false
                }
            }
        ],


    });
    const [date, setDate] = React.useState('2000-01-01');
    const [employmentType, setEmploymentType] = React.useState('Student');
    const [selcit, setSelCit] = React.useState('');
    const [seldis, setSelDis] = React.useState('');
    const [eduDis, setEduDistrict] = React.useState('');
    const [comDis, setComDistrict] = React.useState('');


    const [selEduDis, setSelEduDistrict] = React.useState('');
    const [selComDis, setSelComDistrict] = React.useState('');

    const [eduState, setEduState] = React.useState('');

    const [user_info, setUserInfo] = React.useState({});
    const [location, setLocation] = React.useState([]);
    const [statedata, setStateData] = React.useState('Tamil Nadu');
    const [city, setCity] = React.useState([]);
    const [booth, setBooth] = React.useState([]);

    const [address1, setAddress1] = React.useState('');
    const [address2, setAddress2] = React.useState('');
    const [district, setDistrict] = React.useState('Karur');
    const [pincode, setPincode] = React.useState('');
    const [career, setCareer] = React.useState([]);
    const [fName, setFName] = React.useState('');
    const [lName, setLName] = React.useState('');
    const [mName, setMName] = React.useState('');
    const [birthDate, setBirthDate] = React.useState('');
    const [gender, setGender] = React.useState('Male');
    const [phoneNumber, setPhoneNumber] = React.useState('');
    const [altPhone, setAltPhone] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [altEmail, setAltEmail] = React.useState('');
    const [masterState, setMasterState] = React.useState([]);
    const [masterDistrict, setMasterDistrict] = React.useState([]);
    const [comDistrictMaster, setComDistrictMaster] = React.useState([]);

    const [tempAddress, setTempAddress] = React.useState({
        doorNo: "128",
        street1: "street 1 address",
        street2: "street 2 address",
        city: "namakkal",
        district: "namakkal",
        state: "tamil nadu",
        pincode: "638183",
        abroad: false
    });

    const [perAddress, setPerAddress] = React.useState({
        doorNo: "128",
        street1: "street 1 address",
        street2: "street 2 address",
        city: "namakkal",
        district: "namakkal",
        state: "tamil nadu",
        pincode: "638183",
        abroad: false
    });
    const [intrestedCarrier, setIntrestedCarrier] = React.useState([]);
    const [newCity, setNewCity] = React.useState('');
    const [newState, setNewState] = React.useState('');
    const [newBooth, setNewBooth] = React.useState('');
    const [jwt_token, setJwt] = React.useState('');
    const [userData, setUserData] = React.useState({
        fname: '',
        mName: '',
        lName: '',
        birthDate: new Date(),
        gender: '',
        phoneNumber: '',
        alternatePhone: '',
        email: '',
        alternateEmail: '',
        temporaryAddress: {
            doorNo: "128",
            street1: "street 1 address",
            street2: "street 2 address",
            city: "namakkal",
            district: "namakkal",
            state: "tamil nadu",
            pincode: "638183",
            abroad: false
        },
        permanentAddress: {
            doorNo: "128",
            street1: "street 1 address",
            street2: "street 2 address",
            city: "namakkal",
            district: "namakkal",
            state: "tamil nadu",
            pincode: "638183",
            abroad: false
        },
        city: '',
        state: '',
        booth: '',
        employmentStatus: '',
        intrestedCarrier: [],
        educationDetails: [],
        employmentDetails: [],
        entrepreneurDetails: [],

    });

   

    useEffect(() => {
    let { user } = route.params;
    setUser(user);
    setFName(user.firstName);
    setLName(user.lastName);
    setMName(user.middleName);
    setDate(user.birthDate);
    setPhoneNumber(user.phoneNumber);
    setAltPhone(user.alternatePhone)
    setEmail(user.email);
    setAltEmail(user.alternameEmail);
    setAddress1(user.address1);
    setAddress2(user.address2);
    setGender(user.gender);

        AsyncStorage.getItem('userToken').then(async (res) => {
            const jwt = await res;
            // console.warn("get async", t);
            setJwt(jwt);

            getAllData('get_location/V1.0', jwt)
                .then((response) => {
                    if (response.statusMessage.code == 200) {
                        setLocation(response.data)
                        copyArray(response.data)
                        setIsLoading(false)
                        console.log('location', JSON.stringify(response.data[0].city[0].cityName))
                        // setNewState(response.data[0].stateName)
                        // setNewCity(response.data[0].city[0].cityName)
                        // setStateData(response.data.city)
                        // setBooth(response.data.city.booth)
                    } else {
                        setIsLoading(false)

                    }
                })
                .catch((error) => {
                    setIsLoading(false)

                })
            getAllData('get_state_location/V1.0', jwt)
                .then((response) => {
                    if (response.statusMessage.code == 200) {
                        setMasterState(response.data)
                        setIsLoading(false)
                        console.log('state master', JSON.stringify(masterState))

                    } else {
                        setIsLoading(false)

                    }
                })
                .catch((error) => {
                    setIsLoading(false)

                })

        });

        console.log('location', JSON.stringify(location))

    }, []);

const copyArray = (data) => {
        console.warn("get data", JSON.stringify(data));

        const output = []
        data.map(function (x) {

            output.push(...x.city);
            console.log('city', output);

        })
        console.log('city', output);
        const booth_ = []
        output.map(function (y) {
            booth_.push(...y.booth);
        })
        setBooth(booth_)
        console.log('city', booth_);
    }
     const removeClick = (type) => {
        if (type == "education") {
            let vals = [...values.val];
            vals.splice(this, 1);
            setValues({ val: vals });
        }
        else {
            let emp = [...employment.data];
            emp.splice(this, 1);
            setEmployment({ data: emp });
        }

    }

    const createInputs = () => {

        let pickerComp = null;
        switch (values.val[0].domain) {
            case 'sslc':
                pickerComp = null
                break;
            case 'puc':
                pickerComp =
                    <Picker style={styles.Picker}
                        selectedValue={values.val[0].endingDate}
                        onValueChange={(text) => handleInputChange(text, 0, 'endingDate')}>
                        {
                            values.val[0].domain != '' && <Picker.Item label='Pick Year' value="" />
                        }
                        <Picker.Item label='1 year' value="1" />
                        <Picker.Item label='2 year' value="2" />
                    </Picker>
                break;
            case ('iti' && 'diploma' && 'ba' && 'bsc'):
                pickerComp =
                    <Picker style={styles.Picker}
                        selectedValue={values.val[0].endingDate}
                        onValueChange={(text) => handleInputChange(text, 0, 'endingDate')} >

                        {
                            values.val[0].domain != '' && <Picker.Item label='Pick Year' value="" />
                        }
                        <Picker.Item label='1 year' value="1" />
                        <Picker.Item label='2 year' value="2" />
                        <Picker.Item label='3 year' value="3" />
                    </Picker>
                break;
            case 'mbbs' && 'architecture':
                pickerComp =
                    <Picker style={styles.Picker}
                        selectedValue={values.val[0].endingDate}
                        onValueChange={(text) => handleInputChange(text, 0, 'endingDate')}  >
                        {
                            values.val[0].domain != '' && <Picker.Item label='Pick Year' value="" />
                        }
                        <Picker.Item label='1 year' value="1" />
                        <Picker.Item label='2 year' value="2" />
                        <Picker.Item label='3 year' value="3" />
                        <Picker.Item label='4 year' value="4" />
                        <Picker.Item label='5 year' value="5" />
                    </Picker>
                break;
            default:
                pickerComp =
                    <Picker style={styles.Picker}
                        selectedValue={values.val[0].endingDate}
                        onValueChange={(text) => handleInputChange(text, 0, 'endingDate')}  >
                        {
                            values.val[0].domain != '' && <Picker.Item label='Pick Year' value="" />
                        }
                        <Picker.Item label='1 year' value="1" />
                        <Picker.Item label='2 year' value="2" />
                        <Picker.Item label='3 year' value="3" />
                        <Picker.Item label='4 year' value="4" />
                    </Picker>
        }
        return values.val.map((el, i) =>
            <View style={{ flex: 1, margin: 5, padding: 5, backgroundColor: 'FFFFFF' }} key={i}>

                {/* <TouchableOpacity style={styles.iconContainer} onPress={removeClick.bind(i, 'education')} >
                    <Icon name='close' size={20}
                        color="#F05E23"
                        style={{
                            opacity: 0.7,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderWidth: 1,
                            borderColor: '#fff',
                            borderRadius: 10,
                        }} />
                </TouchableOpacity> */}
                {/* <View>
                    <Text style={styles.textcolor}>Education Type</Text>
                    <DropDownPicker
                        items={[
                            { label: 'School', value: 'school' },
                            { label: 'College', value: 'college' },
                        ]}
                        defaultNull
                        placeholder="Select Education Type"
                        containerStyle={{ height: 40, padding: 5, marginTop: 5 }}
                        onChangeItem={(item) => {
                            const vals = [...values.val];
                            vals[i]['educationType'] = item.value;
                            setValues({ val: vals });
                        }}
                    />
                </View> */}
                <View>
                    {employmentType == "Student" ?
                        <Text style={styles.textcolor}>Present Education</Text>
                        : <Text style={styles.textcolor}>Education Qualification</Text>
                    }
                    <DropDownPicker
                        items={[
                            { label: 'SSLC', value: 'sslc' },
                            { label: 'PUC(+1,+2)', value: 'puc' },
                            { label: 'ITI', value: 'iti' },
                            { label: 'Diploma', value: 'diploma' },
                            { label: 'BA', value: 'ba' },
                            { label: 'BSC', value: 'bsc' },
                            { label: 'BCom', value: 'bcom' },
                            { label: 'BBM', value: 'bbm' },
                            { label: 'BCA', value: 'bca' },
                            { label: 'BBA', value: 'bba' },
                            { label: 'Btech', value: 'btech ' },
                            { label: 'BE', value: 'be' },
                            { label: 'MSC', value: 'msc' },
                            { label: 'MCom', value: 'mcom' },
                            { label: 'MBA', value: 'mba' },
                            { label: 'ME', value: 'me' },
                            { label: 'Mtech', value: 'mtech' },
                            { label: 'MBBS(Doctor)', value: 'mbbs' },
                            { label: 'Architecture', value: 'architecture' },
                            { label: 'PHD', value: 'phd' },
                            { label: 'Nursing', value: 'nursing' },
                            { label: 'Others', value: 'others' },
                        ]}
                        defaultNull
                        placeholder="Select Your Qualification"
                        containerStyle={{ height: 40, padding: 5, marginTop: 5 }}
                        onChangeItem={(item) => {
                            const vals = [...values.val];
                            vals[i]['domain'] = item.value;
                            setValues({ val: vals });
                        }}
                    />
                </View>
                <View>
                    <Text style={styles.textcolor}>School/College Name</Text>
                    <TextInput style={styles.textinput} placeholder="Enter School/College Name" underlineColorAndroid={'transparent'} onChangeText={(text) => handleInputChange(text, i, 'name')} />
                </View>

                {
                    employmentType == 'Student' ?

                        <View style={[styles.textinput, { borderWidth: 1, marginTop: 3 }]}>
                            {
                                values.val[0].domain == 'sslc' ?
                                    null :
                                    <Text style={styles.textcolor}>Current Year <Text style={{ color: 'red', fontSize: 16 }}>*</Text></Text>

                            }
                            {pickerComp}

                        </View>


                        :
                        // <View >
                        //     <Text style={styles.textcolor}>Completed Year</Text>
                        //     <DropDownPicker
                        //         items={[
                        //             { label: '2000', value: '2000' },
                        //             { label: '2001', value: '2001' },
                        //             { label: '2002', value: '2002' },
                        //             { label: '2003', value: '2003' },
                        //             { label: '2004', value: '2004' },
                        //             { label: '2005', value: '2005' },
                        //             { label: '2006', value: '2006' },
                        //             { label: '2007', value: '2007' },
                        //             { label: '2008', value: '2008' },
                        //             { label: '2009', value: '2009' },
                        //             { label: '2010', value: '2010' },
                        //             { label: '2011', value: '2011' },
                        //             { label: '2012', value: '2012' },
                        //             { label: '2013', value: '2013' },
                        //             { label: '2014', value: '2014' },
                        //             { label: '2015', value: '2015' },
                        //             { label: '2016', value: '2016' },
                        //             { label: '2017', value: '2017' },
                        //             { label: '2018', value: '2018' },
                        //             { label: '2019', value: '2019' },
                        //             { label: '2020', value: '2020' }
                        //         ]}
                        //         defaultNull
                        //         placeholder="Select Your Qualification"
                        //         containerStyle={{ height: 40, padding: 5, marginTop: 5,zIndex:2 }}
                        //         onChangeItem={(item) => {
                        //             handleInputChange(item.value, i, 'endingDate')
                        //         }}
                        //     /></View>
                        <>
                            <Text style={styles.textcolor}>Completed Year</Text>
                            <DropDownPicker
                                items={[
                                    { label: '2000', value: '2000' },
                                    { label: '2001', value: '2001' },
                                    { label: '2002', value: '2002' },
                                    { label: '2003', value: '2003' },
                                    { label: '2004', value: '2004' },
                                    { label: '2005', value: '2005' },
                                    { label: '2006', value: '2006' },
                                    { label: '2007', value: '2007' },
                                    { label: '2008', value: '2008' },
                                    { label: '2009', value: '2009' },
                                    { label: '2010', value: '2010' },
                                    { label: '2011', value: '2011' },
                                    { label: '2012', value: '2012' },
                                    { label: '2013', value: '2013' },
                                    { label: '2014', value: '2014' },
                                    { label: '2015', value: '2015' },
                                    { label: '2016', value: '2016' },
                                    { label: '2017', value: '2017' },
                                    { label: '2018', value: '2018' },
                                    { label: '2019', value: '2019' },
                                    { label: '2020', value: '2020' }
                                ]}
                                defaultNull
                                placeholder="Select Your Completion Year"
                                containerStyle={{ height: 40, padding: 5, marginTop: 5 }}
                                onChangeItem={(item) => {
                                    handleInputChange(item.value, i, 'endingDate')
                                }}
                            /></>

                }
                                <View style={{ marginTop: 3 }}>
                    <View style={styles.textinput}>
                        <Text style={styles.Label}>State <Text style={{ color: 'red', fontSize: 16 }}>*</Text></Text>
                        <Picker style={styles.Picker}
                            selectedValue={eduDis}
                            onValueChange={(text) =>
                                (setEduStateArray(text), handleLocationChange(text.stateName, i, 'state'))}
                        >
                            {
                                stateMaster != '' ?
                                    <Picker.Item label='Select state' value="" />
                                    :
                                    <Picker.Item label='eg. Tamil Nadu' value="tn" />
                            }
                            {
                                stateMaster
                            }
                        </Picker>
                    </View>
                </View>
                <View style={{ marginTop: 3 }}>
                    <View style={styles.textinput}>
                        <Text style={styles.Label}>District <Text style={{ color: 'red', fontSize: 16 }}>*</Text></Text>
                        <Picker style={styles.Picker}
                            selectedValue={selEduDis}
                            onValueChange={(text) => {
                                setSelEduDistrict(text), handleLocationChange(text, i, 'city')
                            }}>
                            {
                                districtMaster != '' ?
                                    <Picker.Item label='Select District' value="" />
                                    :
                                    <Picker.Item label='eg. Karur' value="karur" />
                            }
                            {
                                districtMaster
                            }
                        </Picker>
                    </View>
                </View>
                <View>
                    <Text style={styles.textcolor}>Town/City</Text>
                    <TextInput style={styles.textinput} placeholder="Enter City" underlineColorAndroid={'transparent'}
                    // onChangeText={(text) => handleLocationChange(text, i, 'city')} 
                    />
                </View>
                {/* <View>
                    <Text style={styles.textcolor}>State</Text>
                    <TextInput style={styles.textinput} placeholder="Enter State" underlineColorAndroid={'transparent'}
                        onChangeText={(text) => handleLocationChange(text, i, 'state')} />
                </View>
                <View>
                    <Text style={styles.textcolor}>District</Text>
                    <TextInput style={styles.textinput} placeholder="Enter District" underlineColorAndroid={'transparent'}
                        onChangeText={(text) => handleLocationChange(text, i, 'city')} />
                </View> */}


            </View>
        );
    }
     const createEmploymentUI = () => {
        return employment.data.map((el, i) =>
            <View style={{ flex: 1, margin: 5, padding: 5, backgroundColor: 'FFFFFF' }} key={i}>

                {/* <TouchableOpacity style={styles.iconContainer} onPress={removeClick.bind(i, 'employment')} >
                    <Icon name='close' size={20}
                        color="#F05E23"
                        style={{
                            opacity: 0.7,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderWidth: 1,
                            borderColor: '#fff',
                            borderRadius: 10,
                        }} />
                </TouchableOpacity> */}
                <View>
                    <Text style={styles.textcolor}>Company Name:</Text>
                    <TextInput style={styles.textinput} placeholder="Enter your Company Name " underlineColorAndroid={'transparent'}
                        onChangeText={(text) => handleEmpText(text, i, 'comapnyName')}


                    />
                </View>
                <View>
                    <Text style={styles.textcolor}>Designation:</Text>
                    <TextInput style={styles.textinput} placeholder="Enter your Designation "
                        onChangeText={(text) => handleEmpText(text, i, 'designation')}

                        underlineColorAndroid={'transparent'} />
                </View>
                {/* <View>
                    <Text style={styles.textcolor}>Joining Year</Text>
                    <TextInput style={styles.textinput}
                        placeholder="Enter your joining year"
                        underlineColorAndroid={'transparent'}
                        keyboardType='numeric'
                        maxLength={4}
                        onChangeText={(text) => handleEmpText(text, i, 'begins')} />
                </View> */}
                {/* <View>
                    <Text style={styles.textcolor}>Leaving Year</Text>
                    <TextInput style={styles.textinput} placeholder="Enter your leaving year"
                        onChangeText={(text) => handleEmpText(text, i, 'ends')}
                        underlineColorAndroid={'transparent'}
                        keyboardType='numeric'
                        maxLength={4}
                    />
                </View> */}
                <View>
                    <Text style={styles.textcolor}>Total Experience</Text>
                    <TextInput style={styles.textinput} placeholder="Enter total years of experience"
                        onChangeText={(text) => handleEmpText(parseInt(text), i, 'period')}
                        keyboardType='numeric'
                        maxLength={50}
                        underlineColorAndroid={'transparent'} />

                </View>

                <View style={{ marginTop: 3 }}>
                    <View style={styles.textinput}>
                        <Text style={styles.Label}>State <Text style={{ color: 'red', fontSize: 16 }}>*</Text></Text>
                        <Picker style={styles.Picker}
                            selectedValue={comDis}
                            onValueChange={(text) =>
                                (setCompStateArray(text), handleCompanyLoc(text.stateName, i, 'state'))}
                        >
                            {
                                stateMaster != '' ?
                                    <Picker.Item label='Select state' value="" />
                                    :
                                    <Picker.Item label='eg. Tamil nadu' value="tn" />
                            }
                            {
                                stateMaster
                            }
                        </Picker>
                    </View>
                </View>
                <View style={{ marginTop: 3 }}>
                    <View style={styles.textinput}>
                        <Text style={styles.Label}>District <Text style={{ color: 'red', fontSize: 16 }}>*</Text></Text>
                        <Picker style={styles.Picker}
                            selectedValue={selComDis}
                            onValueChange={(text) => {
                                setSelComDistrict(text), handleCompanyLoc(text, i, 'city')
                            }}>
                            {
                                districtComMaster != '' ?
                                    <Picker.Item label='Select District' value="" />
                                    :
                                    <Picker.Item label='eg Karur' value="karur" />
                            }
                            {
                                districtComMaster
                            }
                        </Picker>
                    </View>
                </View>
                <View>
                    <Text style={styles.textcolor}>Town/City</Text>
                    <TextInput style={styles.textinput} placeholder="Enter City" underlineColorAndroid={'transparent'}
                    />
                </View>

            </View>
        );
    }
    const addClick = () => {
        console.log('add moe clicked')
        setValues({
            val: [...values.val, {
                domain: "",
                name: "",
                startingDate: "",
                endingDate: "",
                location: {
                    city: "",
                    state: " ",
                    abroad: false
                }
            }
            ]
        })
    }

    const addCompany = () => {
        console.log('add moe clicked')
        setEmployment({
            data: [...employment.data, {

                comapnyName: "avohi",
                designation: "SDE",
                begins: "2017",
                ends: "2019",
                period: 2,
                location: {
                    city: "bangalore",
                    state: "karnataka",
                    abroad: false
                }

            }
            ]
        })
    }

    const handleText = (event) => {
        console.log('event ', event)
        let vals = [...values.val];
        vals[this] = event;
        setValues({ val: vals });
        console.log('use event ', values)

    }

    const handleEmpText = (value, index, name) => {
        const emp_data = [...employment.data];
        emp_data[index][name] = value;
        setEmployment({ data: emp_data });
    }

    const handleCompanyLoc = (value, index, name) => {
        const emp_data = [...employment.data];
        emp_data[index]['location'][name] = value;
        // setEmployment({ data: emp_data });
        console.log('use emp ', JSON.stringify(employment))

    };

    const handleLocationChange = (value, index, name) => {
        const vals = [...values.val];
        vals[index]['location'][name] = value;
        // setValues({ val: vals });
    };

    const handleInputChange = (value, index, name) => {
        const vals = [...values.val];
        vals[index][name] = value;
        setValues({ val: vals });
    };
    // const handleInputChange = (e, index) => {
    //     const { name, value } = e.target;
    //     const list = [...inputList];
    //     list[index][name] = value;
    //     setInputList(list);
    //   };

    const handleChange = e => {
        const { name, value } = e.target;
        setUserData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const state = { date: "15-05-2016" };


    const uploadUser = () => {

        
        setIsLoading(true)

        if (fName != '' && mName != '' && lName != '' && phoneNumber != '' && email != '' &&
        newState!='' && newCity!='' && location!=''
            && intrestedCarrier != ''&& values.val[0].domain!='') {

            const user_data = {
                firstName: fName,
                middleName: mName,
                lastName: lName,
                birthDate: date,
                gender: gender,
                phoneNumber: phoneNumber,
                alternatePhone: altPhone,
                email: email,
                alternameEmail: altEmail,
                temporaryAddress: tempAddress,
                permanentAddress: perAddress,
                state: statedata,
                ondriyam: newState,
                area: newCity,
                location: newBooth,
                address1: address1,
                address2: address2,
                district: district,
                pincode: pincode,
                employmentStatus: employmentType,
                intrestedCarrier: intrestedCarrier,
                educationDetails: values.val,
                employmentDetails: employment.data,
                
            };
            //console.log('jwt data', JSON.stringify(user_data))
            updateUser('update_user/'+ user.userId + '/V1.0',user_data,jwt_token)
                .then((response) => {
                    console.warn('ressponesee',JSON.stringify(response))
                    if (response.status ==='200') {
                        setIsLoading(false)
                        // console.warn('User Updated successfully');
                        // route.params.onGoBack();
                        //route.params.onGoBack();

                         navigation.popToTop();
                          Alert.alert("User information Updated");

                    }
                    else {
                        
                        setIsLoading(false)

                    }

                })
                .catch((error) => {
                    setIsLoading(false)
                    console.warn('errorrrrr',JSON.stringify(error))
                    Alert.alert("Something went wrong, Try again");

                });

        }
        else {
            setIsLoading(false)
            Alert.alert("Please add all the mandatory fields");
        }
    }

    const setStateArray = (val) => {
        console.log('value array', JSON.stringify(val.ondriyamName))
        if (val != '') {
            setSelDis(val)
            setCity(val.area)
        }
        console.log('value array')
    }

    const selectedCity = (val) => {
        console.log('booth value', JSON.stringify(val.location))
        if (val != '') {
            setSelCit(val)
            setBooth(val.location)
        }
    }

    const setEduStateArray = (val) => {
        // console.log('dist value', JSON.stringify(val.district))
        if (val != '') {
            setEduDistrict(val)
            setMasterDistrict(val.district)
        }
    }
    const setCompStateArray = (val) => {
        // console.log('dist value', JSON.stringify(val.district))
        if (val != '') {
            setComDistrict(val)
            setComDistrictMaster(val.district)
        }
    }




    let locationarray = location.map((s, i) => {
        return <Picker.Item key={i} value={s} label={s.ondriyamName} />
    });
    let stateArray = city.map((s, i) => {
        return <Picker.Item key={i} value={s} label={s.areaName} />
    });
    let boothArray = booth.map((s, i) => {
        return <Picker.Item key={i} value={s.locationName} label={s.locationName} />
    });

    let stateMaster = masterState.map((s, i) => {
        return <Picker.Item key={i} value={s} label={s.stateName} />
    });
    let districtMaster = masterDistrict.map((s, i) => {
        return <Picker.Item key={i} value={s.districtName} label={s.districtName} />
    });

    let districtComMaster = comDistrictMaster.map((s, i) => {
        return <Picker.Item key={i} value={s.districtName} label={s.districtName} />
    });

    if (isLoading) {
        return (
            <View style={styles.regform}>
                <ActivityLoading size="large" />
            </View>
        );
    }
    else {

        return (
            <ScrollView>
                <View style={styles.regform}>

                    {/* <FormInput
                    name='email'
                    value={email}
                    placeholder='Enter your firstname'
                    autoCapitalize='none'
                    onChangeText={()=>{}}
                    iconName='ios-mail'
                    iconColor='#2C384A'
                /> */}
                    <View style={styles.subContainer}>
                        <Text
                            style={styles.subTitle}
                            secureTextEntry={true}>
                            Career Data
                        </Text>
                        <RadioButton.Group onValueChange={value => setEmploymentType(value)} value={employmentType}>
                            <View style={{ marginTop: 3, justifyContent: 'space-evenly', flexDirection: 'row' }}>
                                <View>
                                    <Text>Student</Text>
                                    <RadioButton label="Student" value="Student" />
                                </View>
                                <View>
                                    <Text>Un-Employed</Text>
                                    <RadioButton label="Un-Employed" value="Unemployed" />
                                </View>
                                <View>
                                    <Text>Employed</Text>
                                    <RadioButton label="Employed" value="Employed" />
                                </View>

                            </View>

                        </RadioButton.Group>
                    </View>
                    <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>First Name <Text style={{ color: 'red', fontSize: 16 }}>*</Text> </Text>
                        <TextInput style={styles.textinput}
                         placeholder="Enter your firstName" value={fName}  underlineColorAndroid={'transparent'}
                           
                         onChangeText={(text) => setFName(text)}

                        />
                    </View>
                    <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>Last Name <Text style={{ color: 'red', fontSize: 16 }}>*</Text> </Text>
                        <TextInput style={styles.textinput}
                           placeholder="Enter your lastName" onChangeText={(text) => setLName(text)}
                            value={lName} underlineColorAndroid={'transparent'} />
                    </View>
                    <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>Father Name <Text style={{ color: 'red', fontSize: 16 }}>*</Text> </Text>
                        <TextInput style={styles.textinput}
                            placeholder="Enter your middleName" onChangeText={(text) => setMName(text)}
                            value={mName} underlineColorAndroid={'transparent'} />
                    </View>
                    <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>Date of Birth <Text style={{ color: 'red', fontSize: 16 }}>*</Text> </Text>
                        <DatePicker
                            style={{ width: 250 }}
                           
                            date={date}
                            mode="date"
                            placeholder="select date"
                            format="YYYY-MM-DD"
                            minDate="1990-01-01"
                            maxDate="2021-01-01"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36
                                }
                                // ... You can check the source to find the other keys.
                            }}
                             value={Date}
                            onDateChange={(date) => { setDate(date) }}
                        />
                    </View>
                    <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>Gender <Text style={{ color: 'red', fontSize: 16 }}>*</Text> </Text>
                        <RadioButton.Group onValueChange={value => setGender(value)} value={gender}>
                            <RadioButton.Item label="Male" value="Male" />
                            <RadioButton.Item label="Female" value="Female" />
                            <RadioButton.Item label="Others" value="Others" />
                        </RadioButton.Group>
                    </View>
                    <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>Mobile Number <Text style={{ color: 'red', fontSize: 16 }}>*</Text> </Text>
                        <TextInput style={styles.textinput}
                            value={phoneNumber}
                            onChangeText={(text) => setPhoneNumber(text)}
                            keyboardType='numeric'
                            maxLength={10}
                            placeholder="Enter your Mobile Number" underlineColorAndroid={'transparent'} />
                    </View>
                    <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>Alternate Number </Text>
                        <TextInput style={styles.textinput}
                        value={altPhone}
                            onChangeText={(text) => setAltPhone(text)}
                            keyboardType='numeric'
                            maxLength={10}
                            placeholder="Enter your Alternate Number" underlineColorAndroid={'transparent'} />
                    </View>
                    <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>Email <Text style={{ color: 'red', fontSize: 16 }}>*</Text> </Text>
                        <TextInput style={styles.textinput}
                            onChangeText={(text) => setEmail(text)}
                            value={email}
                            placeholder="Enter your Email" underlineColorAndroid={'transparent'} />
                    </View>
                    <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>Alternate Email</Text>
                        <TextInput style={styles.textinput}
                        value={altEmail}
                            onChangeText={(text) => setAltEmail(text)}
                            placeholder="Enter your Alternate Email" underlineColorAndroid={'transparent'} />
                    </View>
                    {/* <Text style={styles.textcolor}>Temporary Address</Text>

                <View style={styles.textInputContainer}>
                    <Text style={styles.textcolor}>Door No.</Text>
                    <TextInput style={styles.textinput}
                        onChangeText={(text) => setTempAddress(prevState => ({
                            ...prevState,
                            doorNo: text
                        })
                        )}
                        placeholder="Enter your Door no." underlineColorAndroid={'transparent'} />
                </View>

                <View style={styles.textInputContainer}>
                    <Text style={styles.textcolor}>Address - 1 </Text>
                    <TextInput style={styles.textinput}
                        onChangeText={(text) => setTempAddress(prevState => ({
                            ...prevState,
                            street1: text
                        })
                        )}
                        placeholder="Enter your address 1" underlineColorAndroid={'transparent'} />
                </View>
                <View style={styles.textInputContainer}>
                    <Text style={styles.textcolor}>Address - 2 </Text>
                    <TextInput style={styles.textinput}
                        onChangeText={(text) => setTempAddress(prevState => ({
                            ...prevState,
                            street2: text
                        })
                        )}
                        placeholder="Enter your address 2" underlineColorAndroid={'transparent'} />
                </View>


                <Text style={styles.textcolor}>Permanent Address</Text>

                <View style={styles.textInputContainer}>
                    <Text style={styles.textcolor}>Door No.</Text>
                    <TextInput style={styles.textinput}
                        onChangeText={(text) => setPerAddress(prevState => ({
                            ...prevState,
                            doorNo: text
                        })
                        )}
                        placeholder="Enter your Door no." underlineColorAndroid={'transparent'} />
                </View>

                <View style={styles.textInputContainer}>
                    <Text style={styles.textcolor}>Address - 1 </Text>
                    <TextInput style={styles.textinput}
                        onChangeText={(text) => setPerAddress(prevState => ({
                            ...prevState,
                            street1: text
                        })
                        )}
                        placeholder="Enter your address 1" underlineColorAndroid={'transparent'} />
                </View>
                <View style={styles.textInputContainer}>
                    <Text style={styles.textcolor}>Address - 2 </Text>
                    <TextInput style={styles.textinput}
                        onChangeText={(text) => setPerAddress(prevState => ({
                            ...prevState,
                            street2: text
                        })
                        )}
                        placeholder="Enter your address 2" underlineColorAndroid={'transparent'} />
                </View>
                <View style={styles.textInputContainer}>
                    <Text style={styles.textcolor}>Place  </Text>
                    <TextInput style={styles.textinput}
                        onChangeText={(text) => setPerAddress(prevState => ({
                            ...prevState,
                            city: text
                        })
                        )}
                        placeholder="Village / Town" underlineColorAndroid={'transparent'} />
                </View>
                <View style={styles.textInputContainer}>
                    <Text style={styles.textcolor}>District </Text>
                    <TextInput style={styles.textinput}
                        onChangeText={(text) => setPerAddress(prevState => ({
                            ...prevState,
                            district: text
                        })
                        )}
                        placeholder="Enter your district" underlineColorAndroid={'transparent'} />
                </View>

                <View style={styles.textInputContainer}>
                    <Text style={styles.textcolor}>State </Text>
                    <TextInput style={styles.textinput}
                        onChangeText={(text) => setPerAddress(prevState => ({
                            ...prevState,
                            state: text
                        })
                        )}
                        placeholder="Enter your State" underlineColorAndroid={'transparent'} />
                </View>
                <View style={styles.textInputContainer}>
                    <Text style={styles.textcolor}>Pin code </Text>
                    <TextInput style={styles.textinput}
                        onChangeText={(text) => setPerAddress(prevState => ({
                            ...prevState,
                            pincode: text
                        })
                        )}
                        keyboardType='numeric'
                        maxLength={6}
                        placeholder="Enter your pin code" underlineColorAndroid={'transparent'} />
                </View> */}

                    {/* <View style={styles.textInputContainer}>
                    <View style={styles.textinput}>
                        <Text style={styles.Label}>State <Text style={{color:'red' ,fontSize:16}}>*</Text></Text>
                        <Picker style={styles.Picker}
                            selectedValue={seldis}
                            onValueChange={(text) =>
                                (setStateArray(text), setNewState(text.stateName))}>
                            {
                                locationarray != '' ?
                                    <Picker.Item label='Select state' value="" />
                                    :
                                    <Picker.Item label='xyz' value="xyz" />
                            }
                            {
                                locationarray
                            }
                        </Picker>
                    </View>
                </View> */}
                    {/* <View style={styles.textInputContainer}>
                    <View style={styles.textinput}>
                        <Text style={styles.Label}>City <Text style={{color:'red' ,fontSize:16}}>*</Text></Text>
                        <Picker style={styles.Picker}
                            selectedValue={selcit}
                            onValueChange={(text) =>
                                (selectedCity(text), setNewCity(text.cityName))
                            }>
                            {
                                stateArray != '' ?
                                    <Picker.Item label='Select City' value="" />
                                    :
                                    <Picker.Item label='xyz' value="xyz" />
                            }
                            {
                                stateArray
                            }
                        </Picker>
                    </View>
                </View> */}


                    <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>Address - 1 </Text>
                        <TextInput style={styles.textinput}
                        value={address1}
                            onChangeText={(text) => setAddress1(text)}
                            placeholder="Enter your address 1" underlineColorAndroid={'transparent'} />
                    </View>
                    <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>Address - 2 </Text>
                        <TextInput style={styles.textinput}
                        value={address2}
                            onChangeText={(text) => setAddress2(text)}
                            placeholder="Enter your address 2" underlineColorAndroid={'transparent'} />
                    </View>
                    {/* <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>City/Town </Text>
                        <TextInput style={styles.textinput}
                            editable={false}
                            value={newCity}
                            placeholder="Village / Town" underlineColorAndroid={'transparent'} />
                    </View> */}
                    <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>State </Text>
                        <TextInput style={styles.textinput}
                            editable={false}
                            value={statedata}
                            underlineColorAndroid={'transparent'} />

                    </View>
                    <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>District </Text>
                        <TextInput style={styles.textinput}
                            editable={false}

                            value={district}
                            placeholder="Enter your district" underlineColorAndroid={'transparent'} />
                    </View>



                    <View style={styles.textInputContainer}>
                        <View style={styles.textinput}>
                            <Text style={styles.Label}>Ondriyum <Text style={{ color: 'red', fontSize: 16 }}>*</Text></Text>
                            <Picker style={styles.Picker}
                                selectedValue={seldis}
                                onValueChange={(text) =>
                                    (setStateArray(text), setNewState(text.ondriyamName))}>
                                {
                                    locationarray != '' ?
                                        <Picker.Item label='Select Ondriyum' value="" />
                                        :
                                        <Picker.Item label='eg. xyz' value="xyz" />
                                }
                                {
                                    locationarray
                                }
                            </Picker>
                        </View>
                    </View>
                    <View style={styles.textInputContainer}>
                        <View style={styles.textinput}>
                            <Text style={styles.Label}>Area <Text style={{ color: 'red', fontSize: 16 }}>*</Text></Text>
                            <Picker style={styles.Picker}
                                selectedValue={selcit}
                                onValueChange={(text) =>
                                    (selectedCity(text), setNewCity(text.areaName))
                                }>
                                {
                                    stateArray != '' ?
                                        <Picker.Item label='Select Area' value="" />
                                        :
                                        <Picker.Item label='eg. xyz' value="xyz" />
                                }
                                {
                                    stateArray
                                }
                            </Picker>
                        </View>
                    </View>



                    <View style={styles.textInputContainer}>
                        <View style={styles.textinput}>
                            <Text style={styles.Label}>Location <Text style={{ color: 'red', fontSize: 16 }}>*</Text></Text>
                            <Picker style={styles.Picker}
                                selectedValue={newBooth}
                                onValueChange={(text) => {
                                    setNewBooth(text)
                                }

                                }>
                                {
                                    boothArray != '' ?
                                        <Picker.Item label='Select Location' value="" />
                                        :
                                        <Picker.Item label='eg. xyz' value="xyz" />
                                }
                                {
                                    boothArray
                                }
                            </Picker>
                        </View>
                    </View>

                    <View style={styles.textInputContainer}>
                        <Text style={styles.textcolor}>Pin code </Text>
                        <TextInput style={styles.textinput}
                            onChangeText={(text) => setPincode(text)}
                            keyboardType='numeric'
                            maxLength={6}
                            placeholder="Enter your pin code" underlineColorAndroid={'transparent'} />
                    </View>


                    <Text style={styles.textcolor}>Interested Carrier <Text style={{ color: 'red', fontSize: 16 }}>*</Text></Text>
                    <View>
                        <DropDownPicker
                            items={[
                                { label: 'Engineer', value: 'Engineer' },
                                { label: 'Government jobs', value: 'Government jobs' },
                                { label: 'Private Business', value: 'Private Business' },
                                { label: 'Doctor', value: 'Doctor' },
                                { label: 'Teacher', value: 'Teacher' },
                                { label: 'Sports', value: 'Sports' },
                            ]}

                            multiple={true}
                            defaultValue={userData.intrestedCarrier}
                            containerStyle={{
                                height: 40,
                            }} onChangeItem={(item) => {
                                setIntrestedCarrier(item)
                            }
                            }
                        />
                    </View>



                    <View style={{ flex: 1, marginTop: 5 }}>
                        <Text
                            style={styles.subTitle}
                            secureTextEntry={true}>
                            Add Education Details
                    </Text>
                        {createInputs()}
                    </View>
                    {/* <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start', paddingHorizontal: 5 }}>
                    <TouchableOpacity style={styles.addMoreButton}>
                        <Text
                            style={[styles.appButtonText, { fontSize: 14 }]}
                            secureTextEntry={true}
                            color="grey"
                            onPress={() => addClick()}>
                            Add More
                            </Text>
                    </TouchableOpacity>
                </View> */}




                    <View style={styles.subContainer}>

                        {
                            employmentType == 'Employed' &&
                            <View>
                                <Text style={styles.subTitle}>Employment Details:</Text>
                                {
                                    createEmploymentUI()
                                }
                                {/* <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start', paddingHorizontal: 5 }}>
                                <TouchableOpacity style={styles.addMoreButton}>
                                    <Text
                                        style={[styles.appButtonText, { fontSize: 14 }]}
                                        secureTextEntry={true}
                                        color="grey"
                                        onPress={() => addCompany()}>
                                        Add More
                                        </Text>
                                </TouchableOpacity>
                            </View> */}

                            </View>
                        }
                    </View>


                    {/* <Text>{"\n"}</Text>
                <Text>If Presently Studying</Text>
                <Text>{" "}</Text>
                <Text style={styles.textcolor}>Present Education</Text>
                <Text>{" "}</Text>
                <DropDownPicker
                    items={[
                        { label: 'B.E', value: 'be' },
                        { label: 'B.Tech', value: 'btech' },
                        { label: 'B.S.C', value: 'bsc' },
                        { label: 'B.E.D', value: 'bed' },
                        { label: 'M.E', value: 'me' },
                        { label: 'M.Tech', value: 'mtech' },
                    ]}
                    defaultNull
                    placeholder="Select"
                    containerStyle={{ height: 40 }}
                    onChangeItem={(item) => {
                        setUserData({
                            educationDetails: {
                                domain: item
                            }
                        })
                    }}
                />
                <Text>{" "}</Text>
                <Text style={styles.textcolor}>College </Text>
                <TextInput style={styles.textinput}
                    onChangeItem={(item) => {
                        setUserData({
                            educationDetails: {
                                name: item
                            }
                        })
                    }}
                    placeholder="Enter your College name" underlineColorAndroid={'transparent'} />
                <Text style={styles.textcolor}>Place of College </Text>
                <TextInput style={styles.textinput}
                    onChangeItem={(item) => {
                        setUserData(
                            {
                                educationDetails: {
                                    location: {
                                        city: item
                                    }
                                }
                            })
                    }}
                    placeholder="City / Town" underlineColorAndroid={'transparent'} />
                <Text style={styles.textcolor}>Year of Studying </Text>
                <Text>{" "}</Text>
                <DropDownPicker
                    items={[
                        { label: '2010', value: '2010' },
                        { label: '2011', value: '2011' },
                        { label: '2012', value: '2012' },
                        { label: '2013', value: '2013' },
                        { label: '2014', value: '2014' },
                        { label: '2015', value: '2015' },
                        { label: '2016', value: '2016' },
                        { label: '2017', value: '2017' },
                        { label: '2018', value: '2018' },
                        { label: '2019', value: '2019' },
                        { label: '2020', value: '2020' },
                    ]}
                    defaultNull
                    placeholder="Select"
                    containerStyle={{ height: 40 }}
                    onChangeItem={(item) => {
                        setUserData(
                            {
                                educationDetails: {
                                    endingDate: item.value
                                }
                            })
                        console.log('end date', userData.educationDetails.endingDate)
                    }
                    }
                /> */}


                    <View >
                        <TouchableOpacity style={styles.appButtonContainer} onPress={() => uploadUser()}>
                            <Text
                                style={styles.appButtonText}
                                secureTextEntry={true}
                                color="grey"
                                align="center">
                                Submit
                    </Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </ScrollView>
        );
     }






    
    //  const [employmentType, setEmploymentType] = React.useState('Student');
    //  const [fName, setFName] = React.useState('');
    // const [lName, setLName] = React.useState('');
    // const [mName, setMName] = React.useState('');
    //   const [date, setDate] = React.useState('2000-01-01');
    //   const [gender, setGender] = React.useState('Male');
    //   const [phoneNumber, setPhoneNumber] = React.useState('');
    // const [altPhone, setAltPhone] = React.useState('');
    // const [email, setEmail] = React.useState('');
    // const [altEmail, setAltEmail] = React.useState('');
    //     const [address1, setAddress1] = React.useState('');
    // const [address2, setAddress2] = React.useState('');
    // const [statedata, setStateData] = React.useState('Tamil Nadu');
    // const [district, setDistrict] = React.useState('Karur');
    // const [city, setCity] = React.useState([]);
    // const [seldis, setSelDis] = React.useState('');
    
    // const setStateArray = (val) => {
    //     console.log('value array', JSON.stringify(val.ondriyamName))
    //     if (val != '') {
    //         setSelDis(val)
    //         setCity(val.area)
    //     }
    //     console.log('value array')
    // }
    // let locationarray = location.map((s, i) => {
    //     return <Picker.Item key={i} value={s} label={s.ondriyamName} />
    // });






    
    // return(
    //      <ScrollView>
    //             <View style={styles.regform}></View>
    //             <View style={styles.subContainer}>
    //                     <Text
    //                         style={styles.subTitle}
    //                         secureTextEntry={true}>
    //                         Career Data
    //                     </Text>
    //                     <RadioButton.Group onValueChange={value => setEmploymentType(value)} value={employmentType}>
    //                         <View style={{ marginTop: 3, justifyContent: 'space-evenly', flexDirection: 'row' }}>
    //                             <View>
    //                                 <Text>Student</Text>
    //                                 <RadioButton label="Student" value="Student" />
    //                             </View>
    //                             <View>
    //                                 <Text>Un-Employed</Text>
    //                                 <RadioButton label="Un-Employed" value="Unemployed" />
    //                             </View>
    //                             <View>
    //                                 <Text>Employed</Text>
    //                                 <RadioButton label="Employed" value="Employed" />
    //                             </View>

    //                         </View>

    //                     </RadioButton.Group>
    //                     <View style={styles.textInputContainer}>
    //                     <Text style={styles.textcolor}>First Name <Text style={{ color: 'red', fontSize: 16 }}>*</Text> </Text>
    //                     <TextInput style={styles.textinput}
    //                     placeholder={user.firstName} underlineColorAndroid={'transparent'}
    //                         onChangeText={(text) => setFName(text)}

    //                     />
    //                     <View style={styles.textInputContainer}>
    //                     <Text style={styles.textcolor}>Last Name <Text style={{ color: 'red', fontSize: 16 }}>*</Text> </Text>
    //                     <TextInput style={styles.textinput}
    //                         onChangeText={(text) => setLName(text)}
    //                         placeholder={user.lastName} underlineColorAndroid={'transparent'} />
    //                 </View>
    //                 <View style={styles.textInputContainer}>
    //                     <Text style={styles.textcolor}>Father Name <Text style={{ color: 'red', fontSize: 16 }}>*</Text> </Text>
    //                     <TextInput style={styles.textinput}
    //                         onChangeText={(text) => setMName(text)}
    //                         placeholder={user.middleName} underlineColorAndroid={'transparent'} />
    //                 </View>
    //                    <View style={styles.textInputContainer}>
    //                     <Text style={styles.textcolor}>Date of Birth <Text style={{ color: 'red', fontSize: 16 }}>*</Text> </Text>
    //                     <DatePicker
    //                         style={{ width: 250 }}
    //                         date={date}
    //                         mode="date"
    //                         placeholder="select date"
    //                         format="YYYY-MM-DD"
    //                         minDate="1990-01-01"
    //                         maxDate="2021-01-01"
    //                         confirmBtnText="Confirm"
    //                         cancelBtnText="Cancel"
    //                         customStyles={{
    //                             dateIcon: {
    //                                 position: 'absolute',
    //                                 left: 0,
    //                                 top: 4,
    //                                 marginLeft: 0
    //                             },
    //                             dateInput: {
    //                                 marginLeft: 36
    //                             }
    //                             // ... You can check the source to find the other keys.
    //                         }}
    //                         onDateChange={(date) => { setDate(date) }}
    //                     />
    //                 </View>
    //                 <View style={styles.textInputContainer}>
    //                     <Text style={styles.textcolor}>Gender <Text style={{ color: 'red', fontSize: 16 }}>*</Text> </Text>
    //                     <RadioButton.Group onValueChange={value => setGender(value)} value={gender}>
    //                         <RadioButton.Item label="Male" value="Male" />
    //                         <RadioButton.Item label="Female" value="Female" />
    //                         <RadioButton.Item label="Others" value="Others" />
    //                     </RadioButton.Group>
    //                 </View>
    //                 <View style={styles.textInputContainer}>
    //                     <Text style={styles.textcolor}>Mobile Number <Text style={{ color: 'red', fontSize: 16 }}>*</Text> </Text>
    //                     <TextInput style={styles.textinput}
    //                         onChangeText={(text) => setPhoneNumber(text)}
    //                         keyboardType='numeric'
    //                         maxLength={10}
    //                         placeholder="Enter your Mobile Number" underlineColorAndroid={'transparent'} />
    //                 </View>
    //                 <View style={styles.textInputContainer}>
    //                     <Text style={styles.textcolor}>Alternate Number </Text>
    //                     <TextInput style={styles.textinput}
    //                         onChangeText={(text) => setAltPhone(text)}
    //                         keyboardType='numeric'
    //                         maxLength={10}
    //                         placeholder="Enter your Alternate Number" underlineColorAndroid={'transparent'} />
    //                 </View>
    //                 <View style={styles.textInputContainer}>
    //                     <Text style={styles.textcolor}>Email <Text style={{ color: 'red', fontSize: 16 }}>*</Text> </Text>
    //                     <TextInput style={styles.textinput}
    //                         onChangeText={(text) => setEmail(text)}

    //                         placeholder="Enter your Email" underlineColorAndroid={'transparent'} />
    //                 </View>
    //                 <View style={styles.textInputContainer}>
    //                     <Text style={styles.textcolor}>Alternate Email</Text>
    //                     <TextInput style={styles.textinput}
    //                         onChangeText={(text) => setAltEmail(text)}
    //                         placeholder="Enter your Alternate Email" underlineColorAndroid={'transparent'} />
    //                 </View>
    //                 <View style={styles.textInputContainer}>
    //                     <Text style={styles.textcolor}>Address - 1 </Text>
    //                     <TextInput style={styles.textinput}
    //                         onChangeText={(text) => setAddress1(text)}
    //                         placeholder="Enter your address 1" underlineColorAndroid={'transparent'} />
    //                 </View>
    //                 <View style={styles.textInputContainer}>
    //                     <Text style={styles.textcolor}>Address - 2 </Text>
    //                     <TextInput style={styles.textinput}
    //                         onChangeText={(text) => setAddress2(text)}
    //                         placeholder="Enter your address 2" underlineColorAndroid={'transparent'} />
    //                 </View>
    //                 <View style={styles.textInputContainer}>
    //                     <Text style={styles.textcolor}>State </Text>
    //                     <TextInput style={styles.textinput}
    //                         editable={false}
    //                         value={statedata}
    //                         underlineColorAndroid={'transparent'} />

    //                 </View>
    //                   <View style={styles.textInputContainer}>
    //                     <Text style={styles.textcolor}>District </Text>
    //                     <TextInput style={styles.textinput}
    //                         editable={false}

    //                         value={district}
    //                         placeholder="Enter your district" underlineColorAndroid={'transparent'} />
    //                 </View>
    //                 <View style={styles.textInputContainer}>
    //                     <View style={styles.textinput}>
    //                         <Text style={styles.Label}>Ondriyum <Text style={{ color: 'red', fontSize: 16 }}>*</Text></Text>
    //                         <Picker style={styles.Picker}
    //                             selectedValue={seldis}
    //                             onValueChange={(text) =>
    //                                 (setStateArray(text), setNewState(text.ondriyamName))}>
    //                             {
    //                                 locationarray != '' ?
    //                                     <Picker.Item label='Select Ondriyum' value="" />
    //                                     :
    //                                     <Picker.Item label='eg. xyz' value="xyz" />
    //                             }
    //                             {
    //                                 locationarray
    //                             }
    //                         </Picker>
    //                     </View>
    //                 </View>
    //                 </View>
    //                     </View>
    //             </ScrollView>
    // )
}
export default EditUserProfile;
const styles = StyleSheet.create({
    regform: {
        flex: 1,
        justifyContent: 'flex-start',
        padding: 10,
        alignSelf: 'stretch',
        backgroundColor: '#FFFFFF'
    },
    header: {
        fontSize: 30,
        color: '#0f73ee',

        paddingLeft: 60,
    },
    textinput: {
        borderWidth: 1,
        fontSize: 14,
        paddingLeft: 10,
        color: 'black',
        borderColor: '#A9A9A9',
    },
    textView: {

    },
    Loader: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    appButtonContainer: {

        backgroundColor: '#0f73ee',
        borderRadius: 6,
        paddingVertical: 6,
        paddingHorizontal: 10,
        marginBottom: 10,
    },
    addMoreButton: {

        justifyContent: 'flex-start',
        elevation: 3,
        padding: 5,
        backgroundColor: 'gray',
        borderRadius: 1,

    },
    appButtonText: {
        fontSize: 20,
        color: '#fff',
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    iconContainer: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },

    textcolor: {
        fontSize: 12,
        marginTop: 5,
        color: '#808080',
    },
    subTitle: { fontWeight: 'bold', padding: 5, fontSize: 16 },
    subContainer: {
    },
    textInputContainer: {
        padding: 3,
        justifyContent: 'flex-start'
    }
});