// import React from 'react';
// import {
//     Button,
//     Text,
//     StyleSheet,
//     View,
//     FlatList,
//     Image,
//     TouchableOpacity,
//     TouchableHighlight,
//     Modal,
//     Alert,
//     TextInput,
//     Picker, AsyncStorage,
//     ScrollView, ActivityIndicator
// } from 'react-native';

// import DropDownPicker from 'react-native-dropdown-picker';
// import Icons from 'react-native-vector-icons/MaterialIcons';
// import Moment from 'moment';
// import CommonStyles from '../constants/styles'
// import ActivityLoading from '../components/ActivityLoading'
// import { useNavigation } from '@react-navigation/native'

// import AntDesign from 'react-native-vector-icons/AntDesign';

// import { getAllData, addUser } from '../services/Apiservices'
// class Home extends React.Component {
//     state = {
//         isLoading: true,
//         modalVisible: false,
//         dataSource: [],
//         firstName: ' ',
//         gender: ' ',
//         employmentStatus: ' ',
//         boothName: [],
//         arr: [],
//         booths: [],
//         jwt_token: '',
//         boothName: [],
//         count: 0,
//     };

//     setModalVisible = (visible) => {
//         this.setState({ modalVisible: visible });
//     };
//     _onPressBack() {
//         const { goBack } = this.props.navigation;
//         goBack();
//     }
//     onChangeHandle(state, value) {
//         this.setState({
//             [state]: value,
//         });
//     }

//     doLogin() {
//         this.setState({ isLoading: true });

//         const { firstName, gender, employmentStatus, boothName } = this.state;
//         if (this.state.boothName != '') {

//             const req = {
//                 firstName: firstName.trim(' '),
//                 gender: gender.trim(' '),
//                 employmentStatus: employmentStatus,
//                 boothName: this.state.boothName,
//             };

//             addUser('filter_user/V1.0', req, this.state.jwt_token)
//                 .then((response) => {

//                     this.setState({
//                         dataSource: response.data,
//                     });

//                     this.setState({ isLoading: false });

//                 })
//                 .catch((error) => {
//                     this.setState({ isLoading: false });

//                     Alert.alert("Something went wrong, Try again");

//                 });
//             this.setModalVisible(false);

//         }
//         else {
//             this.setState({ isLoading: false });
//             Alert.alert("Please add the Booth Name");
//         }
//     }

//     renderItem = ({ item }) => {
//         return (
//             <TouchableOpacity
//                 style={{ flex: 1, flexDirection: 'row', padding: 5, marginBottom: 3, margin: 5 }}
//                 onPress={() =>
//                     this.props.navigation.navigate('UserDetails', {
//                         book: item,
//                     })}>
//                 <View style={{ flex: 1, elevation: 2, justifyContent: 'flex-start', elevation: 1 }}>
//                     <Text style={styles.text}>
//                         <Image
//                             style={{ height: 20, width: 20 }}
//                             source={{
//                                 uri:
//                                     'https://research.cbc.osu.edu/sokolov.8/wp-content/uploads/2017/12/profile-icon-png-898.png',
//                             }}
//                         />{' '}
//                         {item.firstName}
//                     </Text>
//                     <Text style={styles.text2}>
//                         <Image
//                             style={{ height: 20, width: 20 }}
//                             source={{
//                                 uri:
//                                     'https://www.iconsdb.com/icons/preview/gray/email-2-xxl.png',
//                             }}
//                         />{' '}
//                         {item.email}{' '}
//                     </Text>
//                     <View style={{ flex: 1, justifyContent: 'flex-start', }}>
//                         <Text style={styles.text2}>
//                             <Image
//                                 style={{ height: 20, width: 20 }}
//                                 source={{
//                                     uri:
//                                         'https://cdn3.iconfinder.com/data/icons/maps-and-navigation-colored-1/48/37-512.png',
//                                 }}
//                             />{' '}
//                             {item.permanentAddress.city}{' '}
//                             <Image
//                                 style={{ height: 20, width: 20 }}
//                                 source={{
//                                     uri:
//                                         'https://cdn2.iconfinder.com/data/icons/gaming-and-beyond-part-2-1/80/Phone_gray-512.png',
//                                 }}
//                             />
//                             {item.phoneNumber}
//                         </Text>
//                     </View>
//                 </View>
//                 <View style={styles.dateContainer}>
//                     <Text style={{ fontSize: 12, color: 'white', fontWeight: 'bold' }}>{Moment(item.updatedOn).fromNow()}</Text>
//                 </View>
//             </TouchableOpacity>
//         );
//     };
//     rendersupport = () => {
//         return (
//             <View
//                 style={{ height: 1, width: '100%', backgroundColor: '#8E8E8E' }}></View>
//         );
//     };


//     componentDidMount() {

//         this.props.navigation.setOptions({
//             headerRight: () =>
//              <TouchableOpacity onPress={() => this.setModalVisible(true)} style={{marginRight:10}}>
//                 <Icons name='filter-list' size={25} color='#FFF'/>
//             </TouchableOpacity>
//         })
//         AsyncStorage.getItem('userToken').then(async (res) => {
//             const jwt = await res;
//             this.setState({
//                 jwt_token: jwt,
//             });

//             getAllData('get_location/V1.0', jwt)
//                 .then((response) => {

//                     this.fiter_booth(response.data)
//                     this.setState({ isLoading: false });
//                 })
//                 .catch((error) => {
//                     this.setState({ isLoading: false });

//                 })
//             this.apicall()
//             // this.willFocusSubscription = this.props.navigation.addListener(
//             //     'focus',
//             //     () => {
//             //         this.apicall();
//             //     }
//             // );
//         });

//     }

//     // componentWillUnmount() {
//     //     this.willFocusSubscription.remove();
//     // }

//     apicall = () => {
//         getAllData('retrieve_users/V1.0', this.state.jwt_token)
//             .then((response) => {
//                 this.setState({
//                     dataSource: response.data,
//                 });
//             })
//             .catch((error) => {
//                 this.setState({ isLoading: false });

//             })


//     }
//     fiter_booth = (data) => {
//         console.log('city', data);
//         const output = []
//         data.map(function (x) {

//             output.push(...x.city);
//             console.log('city', output);

//         })
//         console.log('city', output);
//         const booth_ = []
//         output.map(function (y) {
//             booth_.push(...y.booth);
//         })
//         console.log('booth', booth_);

//         this.setState({
//             arr: booth_
//         });
//     }

//     pickerSelect = (data) => {
//         const booth_name = []
//         booth_name.push(data)
//         this.setState({
//             boothName: booth_name
//         });
//     }

//     // const copyArray = (data) => {
//     //     console.warn("get data", JSON.stringify(data));

//     //     const output = []
//     //     data.map(function (x) {

//     //         output.push(...x.city);
//     //         console.log('city', output);

//     //     })
//     //     console.log('city', output);
//     //     const booth_ = []
//     //     output.map(function (y) {
//     //         booth_.push(...y.booth);
//     //     })
//     //     console.log('city', booth_);
//     // }



//     render() {
//         Moment.locale('en');
//         const { modalVisible, arr, booths } = this.state;
//         const { firstName, gender, employmentStatus, boothName, isLoading } = this.state;
//         let boothArray = this.state.arr.map((s, i) => {
//             return <Picker.Item key={i} value={s.boothName} label={s.boothName} />
//         });

//         if (isLoading) {
//             return (
//                 <View style={styles.container}>
//                     <ActivityLoading size="large" />
//                 </View>
//             );
//         }
//         else {
//             return (
//                 <View style={styles.container}>

//                     <Modal visible={modalVisible}
//                         animationType="slide"
//                         transparent={false}>
//                         <ScrollView style={{ backgroundColor: '#000', opacity: 0.6 }}>
//                             <View style={{ flex: 1, margin: 30, borderWidth: 1, backgroundColor: '#FFF', justifyContent: 'center', alignItems: 'center' }} >
//                                 <View
//                                     style={{
//                                         flexDirection: 'row',
//                                         padding: 15,
//                                         borderBottomColor: CommonStyles.color.COLOR_GREY,
//                                         borderBottomWidth: 1

//                                     }}>
//                                     <TouchableOpacity
//                                         style={{ marginLeft: 8, alignItems: 'center' }}
//                                         onPress={() => this.setModalVisible(false)}>
//                                         <Icons name='close' size={30} />

//                                     </TouchableOpacity>

//                                     <View
//                                         style={{
//                                             flex: 1,
//                                             alignItems: 'center',
//                                             justifyContent: 'center',

//                                         }}>
//                                         <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Filter</Text>
//                                     </View>
//                                 </View>

//                                 <View style={styles.regform}>

//                                     <Text style={styles.textcolor}> Name </Text>
//                                     <TextInput
//                                         style={styles.textinput}
//                                         placeholder="Enter your Name"
//                                         underlineColorAndroid={'transparent'}
//                                         value={firstName}
//                                         onChangeText={(value) =>
//                                             this.onChangeHandle('firstName', value)
//                                         }
//                                     />
//                                     <Text style={styles.textcolor}> Gender </Text>
//                                     <DropDownPicker
//                                         items={[
//                                             { label: 'Male', value: 'Male' },
//                                             { label: 'Female', value: 'Female' },
//                                             { label: 'Other', value: 'other' },
//                                         ]}
//                                         defaultNull
//                                         placeholder="Select"
//                                         placeholderColour="#A9A9A9"
//                                         containerStyle={{ height: 40 }}
//                                         onChangeItem={(item) =>
//                                             this.onChangeHandle('gender', item.value)
//                                         }
//                                     />
//                                     <Text>{'\n'}</Text>
//                                     <Text style={styles.textcolor}> EmploymentStatus </Text>
//                                     <DropDownPicker
//                                         items={[
//                                             { label: 'Employed', value: 'Employed' },
//                                             { label: 'Unemployed', value: 'Unemployed' },
//                                             { label: 'Student', value: 'Student' },
//                                         ]}
//                                         defaultNull
//                                         placeholder="Select"
//                                         placeholderColour="#A9A9A9"
//                                         containerStyle={{ height: 40 }}
//                                         onChangeItem={(item) =>
//                                             this.onChangeHandle('employmentStatus', item.value)
//                                         }
//                                     />

//                                     <View style={styles.textInputContainer}>
//                                         <Text style={styles.textcolor}>Booth</Text>
//                                         <Picker style={styles.Picker}
//                                             selectedValue={this.state.boothName}
//                                             onValueChange={(text) => {
//                                                 this.pickerSelect(text)

//                                             }

//                                             }>
//                                             {
//                                                 boothArray != '' ?
//                                                     <Picker.Item label='Select Booth Name' value="" />
//                                                     :
//                                                     <Picker.Item label='xyz' value="xyz" />
//                                             }
//                                             {
//                                                 boothArray
//                                             }
//                                         </Picker>
//                                     </View>


//                                     <TouchableOpacity style={styles.appButtonContainer}>
//                                         <Text
//                                             style={styles.appButtonText}
//                                             secureTextEntry={true}
//                                             color="grey"
//                                             align="center"
//                                             onPress={() => this.doLogin()}>
//                                             Apply
//                                     </Text>
//                                     </TouchableOpacity>
//                                 </View>
//                             </View>
//                         </ScrollView>
//                     </Modal>
//                     {/* <View
//                         style={{
//                             flexDirection: 'row',
//                             paddingHorizontal: 12,
//                             justifyContent: 'center',
//                             alignSelf: 'flex-end',
//                         }}>
//                         <Text
//                             secureTextEntry={true}
//                             color="grey"
//                             align="center">
//                             Filter
//                                     </Text>
//                         <TouchableOpacity onPress={() => this.setModalVisible(true)}>
//                             <Icons name='filter-list' size={25} />
//                         </TouchableOpacity>
//                     </View> */}
//                     <FlatList
//                         data={this.state.dataSource}
//                         renderItem={this.renderItem}
//                         keyExtractor={(item, index) => index}
//                         ItemSeparatorComponent={this.rendersupport}
//                     />

//                     <TouchableOpacity style={styles.fabContainer} onPress={() => this.props.navigation.navigate('SurveyForm')} >
//                         <AntDesign name='addusergroup' size={30} color={'#fff'} backgroundColor={'#fff'} />
//                     </TouchableOpacity>
//                 </View>
//             );

//         }

//     }
// }
// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: CommonStyles.color.COLOR_BACKGROUND_PRIMARY,
//     },
//     text: {
//         marginLeft: 12,
//         fontSize: 20,
//     },
//     text2: {
//         marginLeft: 12,
//         fontSize: 16,
//         color: CommonStyles.color.COLOR_GREY,
//     },
//     regform: {
//         alignSelf: 'stretch',
//         padding: 10
//     },
//     header: {
//         fontSize: 30,
//         color: '#0f73ee',
//         paddingBottom: 5,
//         marginBottom: 20,
//         paddingLeft: 60,
//     },
//     textinput: {
//         alignSelf: 'stretch',
//         height: 40,
//         marginBottom: 30,
//         color: 'black',
//         borderBottomColor: '#A9A9A9',
//         borderBottomWidth: 2,
//     },
//     scrollView: {
//         marginHorizontal: 5,
//         backgroundColor: '#FFFFFF'
//     },
//     centeredView: {
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center',
//         marginTop: 22,
//     },
//     modalView: {
//         margin: 20,
//         backgroundColor: 'white',
//         borderRadius: 20,
//         padding: 35,
//         alignItems: 'center',
//         shadowColor: '#000',
//         shadowOffset: {
//             width: 0,
//             height: 2,
//         },
//         shadowOpacity: 0.25,
//         shadowRadius: 3.84,
//         elevation: 5,
//     },
//     fabContainer: {
//         flex: 1,
//         borderRadius: 60,
//         elevation: 5,
//         padding: 20,
//         backgroundColor: CommonStyles.color.COLOR_PRIMARY,
//         position: 'absolute',
//         bottom: 40,
//         right: 10
//     },

//     appButtonContainer: {
//         elevation: 1,
//         backgroundColor: CommonStyles.color.COLOR_PRIMARY,
//         borderRadius: 6,
//         paddingVertical: 8,
//         paddingHorizontal: 15,
//         margin: 15, paddingBottom: 15,
//     },
//     appButtonText: {
//         fontSize: 25,
//         color: '#fff',
//         fontWeight: 'bold',
//         textAlign: 'center',
//     },
//     openButton: {
//         backgroundColor: '#F194FF',
//         borderRadius: 20,
//         elevation: 1,
//     },
//     textStyle: {
//         color: 'gray',
//         fontWeight: 'bold',
//         textAlign: 'center',
//     },
//     modalText: {
//         marginBottom: 15,
//         textAlign: 'center',
//     },
//     dateContainer: { borderRadius: 15, paddingLeft: 10, paddingRight: 10, backgroundColor: CommonStyles.color.COLOR_GREY_TRANSP, alignSelf: 'flex-start', padding: 5 }
//     ,
//     textcolor: {
//         fontSize: 12,
//         marginTop: 5,
//         color: '#808080',
//     },

//     textInputContainer: {
//         padding: 3,
//         justifyContent: 'flex-start'
//     }
// });
// export default Home;

import React from 'react';
import {
  Button,
  Text,
  StyleSheet,
  View,
  FlatList,
  StatusBar,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  Modal,
  Alert,
  TextInput,
  Picker,
  AsyncStorage,
  ScrollView,
  ActivityIndicator,
} from 'react-native';

import DropDownPicker from 'react-native-dropdown-picker';
import Icons from 'react-native-vector-icons/MaterialIcons';
import Moment from 'moment';
import CommonStyles from '../constants/styles';
import ActivityLoading from '../components/ActivityLoading';
import { useNavigation } from '@react-navigation/native';

import AntDesign from 'react-native-vector-icons/AntDesign';

import { getAllData, addUser } from '../services/Apiservices';
class Home extends React.Component {
  state = {
    isLoading: true,
    modalVisible: false,
    dataSource: [],
    firstName: ' ',
    gender: ' ',
    employmentStatus: ' ',
    boothName: [],
    arr: [],
    booths: [],
    jwt_token: '',
    boothName: [],
    count: 0,
  };

  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible });
  };
  _onPressBack() {
    const { goBack } = this.props.navigation;
    goBack();
  }
  onChangeHandle(state, value) {
    this.setState({
      [state]: value,
    });
  }

  doLogin() {
    this.setState({ isLoading: true });

    const { firstName, gender, employmentStatus, boothName } = this.state;
    if (this.state.boothName != '') {
      const req = {
        firstName: firstName.trim(' '),
        gender: gender.trim(' '),
        employmentStatus: employmentStatus,
        boothName: this.state.boothName,
      };

      addUser('filter_user/V1.0', req, this.state.jwt_token)
        .then((response) => {
          this.setState({
            dataSource: response.data,
          });

          this.setState({ isLoading: false });
        })
        .catch((error) => {
          this.setState({ isLoading: false });

          Alert.alert('Something went wrong, Try again');
        });
      this.setModalVisible(false);
    } else {
      this.setState({ isLoading: false });
      Alert.alert('Please add the Booth Name');
    }
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: 10,
          paddingVertical: 10,
        }}
        onPress={() =>
          this.props.navigation.navigate('UserDetails', {
            book: item,
           onGoBack : () => this.refresh(),
          })
        }
      >
        <View
          style={{
            flex: 1,
            elevation: 2,
            justifyContent: 'flex-start',
            elevation: 1,
          }}
        >
          <Text style={styles.text}>
            <Image
              style={{ height: 20, width: 20 }}
              source={{
                uri:
                  'https://research.cbc.osu.edu/sokolov.8/wp-content/uploads/2017/12/profile-icon-png-898.png',
              }}
            />{' '}
            {item.firstName}
          </Text>
          <Text style={styles.text2}>
            <Image
              style={{ height: 15, width: 15 }}
              source={{
                uri:
                  'https://www.iconsdb.com/icons/preview/gray/email-2-xxl.png',
              }}
            />{' '}
            - {item.email}{' '}
          </Text>
          <View style={{ flex: 1, justifyContent: 'flex-start' }}>
            <Text style={styles.text2}>
              <Image
                style={{ height: 15, width: 15 }}
                source={{
                  uri:
                    'https://cdn3.iconfinder.com/data/icons/maps-and-navigation-colored-1/48/37-512.png',
                }}
              />{' '}
              - {item.ondriyam},{' '}
              <Image
                style={{ height: 12, width: 12 }}
                source={{
                  uri:
                    'https://cdn2.iconfinder.com/data/icons/gaming-and-beyond-part-2-1/80/Phone_gray-512.png',
                }}
              />
              - {item.phoneNumber}
            </Text>
          </View>
        </View>
        <View style={styles.dateContainer}>
          <Text style={{ fontSize: 11, color: 'white', fontWeight: 'bold' }}>
            {Moment(item.updatedOn).fromNow()}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  rendersupport = () => {
    return (
      <View
        style={{ height: 1, width: '100%', backgroundColor: '#8E8E8E' }}
      ></View>
    );
  };

  componentDidMount() {

      this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.apicall();
    });
    
    this.props.navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          onPress={() => this.setModalVisible(true)}
          style={{ marginRight: 10 }}
        >
          <Icons name="filter-list" size={25} color="#FFF" />
        </TouchableOpacity>
      ),
    });
    AsyncStorage.getItem('userToken').then(async (res) => {
      const jwt = await res;
      this.setState({
        jwt_token: jwt,
      });
      
      getAllData('get_location/V1.0', jwt)
        .then((response) => {
          this.fiter_booth(response.data);
          this.setState({ isLoading: false });
        })
        .catch((error) => {
          this.setState({ isLoading: false });
        });
      this.apicall();
      // this.willFocusSubscription = this.props.navigation.addListener(
      //     'focus',
      //     () => {
      //         this.apicall();
      //     }
      // );
    });
  }

  componentWillUnmount() {
      this._unsubscribe();
  }

  apicall = () => {
    getAllData('retrieve_users/V1.0', this.state.jwt_token)
      .then((response) => {
        this.setState({
          dataSource: response.data,
        });
      })
      .catch((error) => {
        this.setState({ isLoading: false });
      });
  };
  fiter_booth = (data) => {
    console.log('city', data);
    const output = [];
    data.map(function (x) {
      output.push(...x.area);
      console.log('city', output);
    });
    console.log('city', output);
    const booth_ = [];
    output.map(function (y) {
      booth_.push(...y.location);
    });
    console.log('booth', booth_);

    this.setState({
      arr: booth_,
    });
  };

  pickerSelect = (data) => {
    const booth_name = [];
    booth_name.push(data);
    this.setState({
      boothName: booth_name,
    });
  };

  refresh = () => {
    this.apicall();
  }

  // const copyArray = (data) => {
  //     console.warn("get data", JSON.stringify(data));

  //     const output = []
  //     data.map(function (x) {

  //         output.push(...x.city);
  //         console.log('city', output);

  //     })
  //     console.log('city', output);
  //     const booth_ = []
  //     output.map(function (y) {
  //         booth_.push(...y.booth);
  //     })
  //     console.log('city', booth_);
  // }

  render() {
    Moment.locale('en');
    const { modalVisible, arr, booths } = this.state;
    const {
      firstName,
      gender,
      employmentStatus,
      boothName,
      isLoading,
    } = this.state;
    
    let boothArray = this.state.arr.map((s, i) => {
      return <Picker.Item key={i} value={s.locationName} label={s.locationName} />;
    });

    if (isLoading) {
      return (
        <View style={styles.container}>
          <StatusBar backgroundColor="#0080FE" barStyle="light-content" />
          <ActivityLoading size="large" />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <StatusBar backgroundColor="#0080FE" barStyle="light-content" />
          <Modal
            visible={modalVisible}
            animationType="slide"
            transparent={false}
          >
            <ScrollView style={{ backgroundColor: '#000', opacity: 0.6 }}>
              <View
                style={{
                  flex: 1,
                  margin: 30,
                  borderWidth: 1,
                  backgroundColor: '#FFF',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <View
                  style={{
                    flexDirection: 'row',
                    padding: 15,
                    borderBottomColor: CommonStyles.color.COLOR_GREY,
                    borderBottomWidth: 1,
                  }}
                >
                  <TouchableOpacity
                    style={{ marginLeft: 8, alignItems: 'center' }}
                    onPress={() => this.setModalVisible(false)}
                  >
                    <Icons name="close" size={30} />
                  </TouchableOpacity>

                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
                      Filter
                    </Text>
                  </View>
                </View>

                <View style={styles.regform}>
                  <Text style={styles.textcolor}> Name </Text>
                  <TextInput
                    style={styles.textinput}
                    placeholder="Enter your Name"
                    underlineColorAndroid={'transparent'}
                    value={firstName}
                    onChangeText={(value) =>
                      this.onChangeHandle('firstName', value)
                    }
                  />
                  <Text style={styles.textcolor}> Gender </Text>
                  <DropDownPicker
                    items={[
                      { label: 'Male', value: 'Male' },
                      { label: 'Female', value: 'Female' },
                      { label: 'Other', value: 'other' },
                    ]}
                    defaultNull
                    placeholder="Select"
                    placeholderColour="#A9A9A9"
                    containerStyle={{ height: 40 }}
                    onChangeItem={(item) =>
                      this.onChangeHandle('gender', item.value)
                    }
                  />
                  <Text>{'\n'}</Text>
                  <Text style={styles.textcolor}> EmploymentStatus </Text>
                  <DropDownPicker
                    items={[
                      { label: 'Employed', value: 'Employed' },
                      { label: 'Unemployed', value: 'Unemployed' },
                      { label: 'Student', value: 'Student' },
                    ]}
                    defaultNull
                    placeholder="Select"
                    placeholderColour="#A9A9A9"
                    containerStyle={{ height: 40 }}
                    onChangeItem={(item) =>
                      this.onChangeHandle('employmentStatus', item.value)
                    }
                  />

                  <View style={styles.textInputContainer}>
                    <Text>{'\n'}</Text>
                    <Text style={styles.textcolor}>Booth</Text>
                    <Picker
                      style={styles.Picker}
                      selectedValue={this.state.boothName[0]}
                      onValueChange={(text) => {
                        this.pickerSelect(text);
                      }}
                    >
                      {boothArray != '' ? (
                        <Picker.Item label="Select Booth Name" value="" />
                      ) : (
                          <Picker.Item label="xyz" value="xyz" />
                        )}
                      {boothArray}
                    </Picker>
                  </View>

                  <TouchableOpacity style={styles.appButtonContainer}>
                    <Text
                      style={styles.appButtonText}
                      secureTextEntry={true}
                      color="grey"
                      align="center"
                      onPress={() => this.doLogin()}
                    >
                      Apply
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </Modal>
          {/* <View
                        style={{
                            flexDirection: 'row',
                            paddingHorizontal: 12,
                            justifyContent: 'center',
                            alignSelf: 'flex-end',
                        }}>
                        <Text
                            secureTextEntry={true}
                            color="grey"
                            align="center">
                            Filter
                                    </Text>
                        <TouchableOpacity onPress={() => this.setModalVisible(true)}>
                            <Icons name='filter-list' size={25} />
                        </TouchableOpacity>
                    </View> */}
          
          <FlatList
            data={this.state.dataSource}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => index}
            ItemSeparatorComponent={this.rendersupport}
          />

          <TouchableOpacity
            style={styles.fabContainer}
            onPress={() => this.props.navigation.navigate('SurveyForm', {
              onGoBack: () => this.refresh(),
            })}
          >
            <AntDesign
              name="addusergroup"
              size={30}
              color={'#fff'}
              backgroundColor={'#fff'}
            />
          </TouchableOpacity>
        </View>
      );
    }
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: CommonStyles.color.COLOR_BACKGROUND_PRIMARY,
  },
  text: {
    marginLeft: 12,
    textTransform: 'uppercase',
    fontSize: 18,
    paddingVertical: 2,
  },
  text2: {
    marginLeft: 12,
    fontSize: 14,
    color: CommonStyles.color.COLOR_GREY,
  },
  regform: {
    alignSelf: 'stretch',
    padding: 10,
  },
  header: {
    fontSize: 30,
    color: '#0f73ee',
    paddingBottom: 5,
    marginBottom: 20,
    paddingLeft: 60,
  },
  textinput: {
    alignSelf: 'stretch',
    height: 40,
    marginBottom: 30,
    color: 'black',
    borderBottomColor: '#A9A9A9',
    borderBottomWidth: 2,
  },
  scrollView: {
    marginHorizontal: 5,
    backgroundColor: '#FFFFFF',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  fabContainer: {
    flex: 1,
    borderRadius: 60,
    elevation: 5,
    padding: 20,
    backgroundColor: CommonStyles.color.COLOR_PRIMARY,
    position: 'absolute',
    bottom: 40,
    right: 10,
  },

  appButtonContainer: {
    backgroundColor: CommonStyles.color.COLOR_PRIMARY,
    borderRadius: 6,
    paddingVertical: 8,
    paddingHorizontal: 15,
    margin: 15,
    marginTop:25
  },
  appButtonText: {
    fontSize: 25,
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    elevation: 1,
  },
  textStyle: {
    color: 'gray',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  dateContainer: {
    borderRadius: 15,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: CommonStyles.color.COLOR_GREY_TRANSP,
    alignSelf: 'flex-start',
    padding: 5,
  },
  textcolor: {
    fontSize: 12,
    marginTop: 5,
    color: '#808080',
  },

  textInputContainer: {
    padding: 3,
    justifyContent: 'flex-start',
  },
});
export default Home;