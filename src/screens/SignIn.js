// import React, { useEffect } from 'react';
// import {
//     View,
//     Text,
//     TouchableOpacity, AsyncStorage,
//     TextInput,
//     Platform,
//     StyleSheet,
//     StatusBar,
//     Alert,
//     Image,
//     ScrollView,
// } from 'react-native';
// import * as Animatable from 'react-native-animatable';
// import LinearGradient from 'react-native-linear-gradient';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';
// import Feather from 'react-native-vector-icons/Feather';
// import { postMethod } from '../services/Apiservices'
// import { useTheme } from 'react-native-paper';
// import { AuthContext } from '../routes'
// import ActivityLoading from '../components/ActivityLoading';
// const SignIn = ({ navigation }) => {
//     const [data, setData] = React.useState({
//         username: '',
//         password: '',
//         check_textInputChange: false,
//         secureTextEntry: true,
//         isValidUser: true,
//         isValidPassword: true,
//     });

//     const [isLoading, setIsLoading] = React.useState(false);


//     const { signIn } = React.useContext(AuthContext);

//     const setInfo = async (data) => {
//         console.log(data.data)
//         await AsyncStorage.setItem('userInfo', data.data);
//     }

//     const doLogin = () => {
//         const req = {
//             userName: data.username,
//             password: data.password,
//         };

//         if (data.username != '') {
//             setIsLoading(true)
//             postMethod('login/V1.0', req)
//                 .then((response) => {
//                     if (response) {
//                         console.warn("login response", response);

//                         if (response.status == 200) {

//                             const user_data = {
//                                 token: response.data.token,
//                                 userId: response.data.userId,
//                                 roles: response.data.roles,
//                                 userName: response.data.userName,
//                             };

//                             setInfo(response)
//                             signIn(user_data);
//                             setIsLoading(false)

//                             Alert.alert('Login successful');
//                         }
//                        else if (response.status == 500) {
//                         setIsLoading(false)

//                             Alert.alert('Not able to login in, Please try later');
//                         }
//                         if (response.statuscode == 404) {
//                             setIsLoading(false)

//                             Alert.alert('User account already deactivated');
//                         }
//                     }

//                 })
//                 .catch((error) => {
//                     setIsLoading(false)

//                     Alert.alert('No Internet connection.\n Please check your internet connection \nor try again', error);
//                     console.warn('No Internet connection.\n Please check your internet connection \nor try again', error);
//                 });
//         }
//         else {
//             setIsLoading(false)

//             Alert.alert('Username and Password cannot be empty');
//         }



//     };

//     const { colors } = useTheme();
//     const handlePasswordChange = (val) => {
//         if (val.trim().length >= 3) {
//             setData({
//                 ...data,
//                 password: val,
//                 isValidPassword: true,
//             });
//         } else {
//             setData({
//                 ...data,
//                 password: val,
//                 isValidPassword: false,
//             });
//         }
//     };
//     const updateSecureTextEntry = () => {
//         setData({
//             ...data,
//             secureTextEntry: !data.secureTextEntry,
//         });
//     };

//     const handleValidUser = (val) => {
//         if (val.trim().length >= 1) {
//             setData({
//                 ...data,
//                 username: val,
//                 isValidUser: true,
//             });
//         } else {
//             setData({
//                 ...data,
//                 isValidUser: false,
//             });
//         }
//     };

//     return (
//         <ScrollView style={{backgroundColor:'#fff'}}>
//             <View style={styles.container}>
//                 <StatusBar backgroundColor="#009387" barStyle="light-content" />
//                 <View style={styles.header}>
//                     <Text style={styles.text_header}>Welcome!</Text>
//                     <Image
//                         source={require('../assets/images/login_screen.jpg')}
//                         resizeMode="contain"
//                         style={{
//                             width: 125,
//                             height: 125,
//                         }}
//                     />
//                 </View>
//                 <Animatable.View
//                     animation="fadeInUpBig"
//                     style={[
//                         styles.footer,
//                         {
//                             backgroundColor: colors.background,
//                         },
//                     ]}>
//                     <Text
//                         style={[
//                             styles.text_footer,
//                             {
//                                 color: colors.text,
//                             },
//                         ]}>
//                         Username
//                     </Text>
//                     <View style={styles.action}>
//                         <FontAwesome name="user-o" color={colors.text} size={20} />
//                         <TextInput
//                             placeholder="Your Username"
//                             placeholderTextColor="#666666"
//                             style={[
//                                 styles.textInput,
//                                 {
//                                     color: colors.text,
//                                 },
//                             ]}
//                             autoCapitalize="none"
//                             onChangeText={(val) => handleValidUser(val)}
//                         />
//                     </View>

//                     <Text
//                         style={[
//                             styles.text_footer,
//                             {
//                                 color: colors.text,
                               
//                             },
//                         ]}>
//                         Password
//           </Text>
//                     <View style={styles.action}>
//                         <Feather name="lock" color={colors.text} size={20} />
//                         <TextInput
//                             placeholder="Your Password"
//                             placeholderTextColor="#666666"
//                             secureTextEntry={data.secureTextEntry ? true : false}
//                             style={[
//                                 styles.textInput,
//                                 {
//                                     color: colors.text,
//                                 },
//                             ]}
//                             autoCapitalize="none"
//                             onChangeText={(val) => handlePasswordChange(val)}
//                         />
//                         <TouchableOpacity onPress={updateSecureTextEntry}>
//                             {data.secureTextEntry ? (
//                                 <Feather name="eye-off" color="grey" size={20} />
//                             ) : (
//                                     <Feather name="eye" color="grey" size={20} />
//                                 )}
//                         </TouchableOpacity>
//                     </View>
//                     {data.isValidPassword ? null : (
//                         <Animatable.View animation="fadeInLeft" duration={500}>
//                             <Text style={styles.errorMsg}>
//                                 Password must be 8 characters long.
//               </Text>
//                         </Animatable.View>
//                     )}

//                     <TouchableOpacity onPress={() => console.log('forgotpassword')}>
//                         <Text style={{ color: '#009387', marginTop: 15 }}>
//                             Forgot password?
//             </Text>
//                     </TouchableOpacity>
//                     <Text> </Text>
//                     <Text> </Text>

//                     <TouchableOpacity style={styles.appButtonContainer}   onPress={() => doLogin()}>
//                         <Text
//                             style={styles.appButtonText}
//                             secureTextEntry={true}
//                             color="grey"
//                             align="center">
//                             SIGN IN
//             </Text>
//                     </TouchableOpacity>
//                     {
//                         isLoading
//                         ?
//                         <ActivityLoading size='large' />
//                         :null
//                     }
//                 </Animatable.View>
//             </View>
//         </ScrollView>
//     );
// };

// export default SignIn;
// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: '#fff',
//         justifyContent: 'flex-start',
//         alignItems: 'center'
//     },
//     header: {
//         flex: 1,
//         justifyContent: 'flex-end',
//         paddingHorizontal: 20,
//         paddingTop: 50,
//         backgroundColor: '#fff',
//     },
//     footer: {
//         flex: 1,
//         elevation: 2,
        
//         borderTopLeftRadius: 30,
//         borderTopRightRadius: 30,
//         paddingHorizontal: 20,
//         paddingVertical: 30,
//         marginBottom:30
//     },
//     text_header: {
//         color: '#0f73ee',
//         fontWeight: 'bold',
//         fontSize: 30,
//     },
//     text_footer: {
//         color: '#05375a',
//         fontSize: 18,
//     },
//     action: {
//         flexDirection: 'row',
//         justifyContent:'flex-start',
//         marginTop: 10,
//         borderBottomColor: '#f2f2f2',
//         paddingBottom: 5,
//     },
//     textInput: {
//         flex: 1,
//         marginTop: Platform.OS === 'ios' ? 0 : -12,
//         paddingLeft: 10,
//         color: '#05375a',
//     },
//     errorMsg: {
//         color: '#FF0000',
//         fontSize: 14,
//     },
//     appButtonContainer: {
//         elevation: 1,
//         backgroundColor: '#0f73ee',
//         borderRadius: 6,
//         paddingVertical: 8,
//         paddingHorizontal: 15,
//         width: 300,
//         height: 50,
//         paddingBottom: 20,
//     },
//     appButtonText: {
//         fontSize: 20,
//         color: '#fff',
//         fontWeight: 'bold',
//         textAlign: 'center',
//     },
// });

import React, { useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  AsyncStorage,
  TextInput,
  Platform,
  StyleSheet,
  StatusBar,
  Alert,
  Image,
  ScrollView,
  ImageBackground,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import { postMethod } from '../services/Apiservices';
import { useTheme } from 'react-native-paper';
import { AuthContext } from '../routes';
import ActivityLoading from '../components/ActivityLoading';
const SignIn = ({ navigation }) => {
  const [data, setData] = React.useState({
    username: '',
    password: '',
    check_textInputChange: false,
    secureTextEntry: true,
    isValidUser: true,
    isValidPassword: true,
    TextInputDisableStatus: false,
    mobileNo: '',
    isValidMobileNo: true,
  });

  const [isLoading, setIsLoading] = React.useState(false);

  const { signIn } = React.useContext(AuthContext);

  const setInfo = async (data) => {
    console.log(data.data);
    await AsyncStorage.setItem('userInfo', data.data);
  };

  const doLogin = () => {

    const req = {
      userName: data.username,
      password: data.password,
    };

    if (data.username != '') {
      setIsLoading(true);
      postMethod('login/V1.0', req)
        .then((response) => {
          if (response) {
            console.warn('login response', response);

            if (response.status == 200) {
              const user_data = {
                token: response.data.token,
                userId: response.data.userId,
                roles: response.data.roles,
                userName: response.data.userName,
              };

              setInfo(response);
              signIn(user_data);
              setIsLoading(false);

              Alert.alert('Login successful');
            } else if (response.status == 500) {
              setIsLoading(false);
              Alert.alert('Not able to login in, Please try later');
            }
            if (response.statuscode == 404) {
              setIsLoading(false);
              Alert.alert('User account already deactivated');
            }
          }
        })
        .catch((error) => {
          setIsLoading(false);
          Alert.alert(
            'No Internet connection.\n Please check your internet connection \nor try again',
            error
          );
          console.warn(
            'No Internet connection.\n Please check your internet connection \nor try again',
            error
          );
        });
    }    
     else {
      setIsLoading(false);
      Alert.alert('Username and Password cannot be empty');
    }
  };
  const { colors } = useTheme();
  const handlePasswordChange = (val) => {
    if (val.trim().length >= 3) {
      setData({
        ...data,
        password: val,
        isValidPassword: true,
      });
    } else {
      setData({
        ...data,
        password: val,
        isValidPassword: false,
      });
    }
  };
  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  const handleValidUser = (val) => {
    // let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    // if (reg.test(val) === false) {
    if (val.trim().length >= 1) {
      setData({
        ...data,
        username: val,
        isValidUser: true,
      });
    } else {
      setData({
        ...data,
        isValidUser: false,
      });
    }
  };

  const handleValidMobile = (val) => {
    if (val.trim().length >= 10) {
      setData({
        ...data,
        mobileNo: val,
        isValidMobileNo: true,
      });
    } else {
      setData({
        ...data,
        isValidMobileNo: false,
      });
    }
  };

  return (
    <ScrollView style={{ backgroundColor: '#fff' }}>
      <View style={styles.container}>
        <StatusBar backgroundColor="#0080FE" barStyle="light-content" />
        <View style={styles.header}>
          {/* <Text style={styles.text_header}>Welcome!</Text> */}
          <Image
            source={require('../assets/images/login_screen.jpg')}
            resizeMode="contain"
            style={{
              width: 125,
              height: 125,
              justifyContent: 'center',
              alignItems: 'center',
              textAlign: 'center',
            }}
          />
          <Text
            style={[{ fontWeight: 'bold', fontSize: 30 }, styles.text_header]}
          >
            Welcome Back!
          </Text>
          <Text
            style={{
              color: '#0080FE',
            }}
          >
            Sign in to continue
          </Text>
        </View>

        <Animatable.View animation="fadeInUpBig" style={styles.footer}>
          {/* <Text
            style={[
              styles.text_footer,
              {
                color: colors.text,
              },
            ]}
          >
            Username
          </Text> */}
          <View style={styles.action1}>
            {/* <MaterialCommunityIcons
              name="email-outline"
              color={colors.text}
              size={20}
            /> */}
            <FontAwesome name="user-o" color={colors.text} size={20} />
            <TextInput
              placeholder="UserName"
              placeholderTextColor="#666666"
              //   editable={false}
              //   selectTextOnFocus={false}
              // keyboardType="email-address"
              style={[
                styles.textInput,
                {
                  color: colors.text,
                },
              ]}
              autoCapitalize="none"
              onChangeText={(val) => handleValidUser(val)}
            />
          </View>
          {data.isValidUser ? null : (
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={styles.errorMsg}>Enter Your Valid UserName.</Text>
            </Animatable.View>
          )}

          {/* <View style={styles.action1}>
            <FontAwesome5 name="mobile-alt" color={colors.text} size={20} />
            <TextInput
              placeholder="Mobile Number"
              placeholderTextColor="#666666"
              autoCapitalize="none"
              style={[
                styles.textInput,
                {
                  color: colors.text,
                },
              ]}
              keyboardType="numeric"
              maxLength={10}
              onChangeText={(val) => handleValidMobile(val)}
            />
          </View> */}

          {/* <Text
            style={[
              styles.text_footer,
              {
                color: colors.text,
              },
            ]}
          >
            Password
          </Text> */}
          <View style={styles.action1}>
            <Feather name="lock" color={colors.text} size={20} />
            <TextInput
              placeholder="Your Password"
              placeholderTextColor="#666666"
              secureTextEntry={data.secureTextEntry ? true : false}
              style={[
                styles.textInput,
                {
                  color: colors.text,
                },
              ]}
              autoCapitalize="none"
              onChangeText={(val) => handlePasswordChange(val)}
            />
            <TouchableOpacity onPress={updateSecureTextEntry}>
              {data.secureTextEntry ? (
                <Feather name="eye-off" color="grey" size={20} />
              ) : (
                <Feather name="eye" color="grey" size={20} />
              )}
            </TouchableOpacity>
          </View>
          {data.isValidPassword ? null : (
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={styles.errorMsg}>
                Password must be 8 characters long.
              </Text>
            </Animatable.View>
          )}

          {/* <TouchableOpacity onPress={() => console.log('forgotpassword')}>
            <Text style={{ color: '#009387', marginTop: 15 }}>
              Forgot password?
            </Text>
          </TouchableOpacity> */}
          <Text> </Text>
          <Text> </Text>

          <TouchableOpacity
            style={[
              styles.appButtonContainer,
              {
                marginHorizontal: 20,
              },
            ]}
            onPress={() => doLogin()}
          >
            <Text
              style={styles.appButtonText}
              secureTextEntry={true}
              color="grey"
              align="center"
            >
              SIGN IN
            </Text>
          </TouchableOpacity>

          <Text
            style={{
              marginTop: 20,
              color: '#0080FE',
              textAlign: 'center',
              paddingHorizontal: 20,
            }}
          >
            Please enter your register Email Address & Password.
          </Text>
          {isLoading ? <ActivityLoading size="large" /> : null}
        </Animatable.View>
      </View>
    </ScrollView>
  );
};

export default SignIn;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 50,
    paddingHorizontal: 30,
  },

  imageBackground: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  header: {
    flex: 1,
    paddingHorizontal: 10,
    // paddingTop: 50,
    paddingVertical: 50,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    // elevation: 2,
    // borderTopLeftRadius: 30,
    // borderTopRightRadius: 30,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginBottom: 30,
  },
  text_header: {
    color: '#0080FE',
    fontWeight: 'bold',
    fontSize: 30,
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 10,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },

  textInput: {
    flex: 1,
    // marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
    paddingVertical: 5,
  },
  appButtonContainer: {
    elevation: 1,
    backgroundColor: '#0080FE',
    borderRadius: 3,
    paddingVertical: 10,
    paddingHorizontal: 10,
    textAlign: 'center',
    width: 300,
    height: 50,
    paddingBottom: 20,
  },
  appButtonText: {
    fontSize: 20,
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  action1: {
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 5,
    alignItems: 'center',
    borderColor: '#0080FE',
    marginTop: 10,
  },
  textInput1: {
    flex: 1,
    paddingLeft: 10,
  },
  footer1: {
    flex: 2,
    padding: 20,
  },
});