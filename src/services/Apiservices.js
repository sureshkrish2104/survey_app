// export const URL = 'https://survey-management-app.herokuapp.com/';
// export const URL='http://ec2-13-233-252-119.ap-south-1.compute.amazonaws.com:8090/';
export const URL='http://13.232.86.249:8090/';
//  export const URL='https://five16.com';
export const jwt='eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJib290aG1lbWJlcjEiLCJleHAiOjE2MDcxODU0NDcsImlhdCI6MTYwNzE0OTQ0N30.kGIJemszMQh4t3Lr1znEDE5Os9_4OLvO5Py77sxtI20'
export const getAllData = (type,jwt_token) => {
    console.warn('jwt',type,jwt_token);
    let data = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization':`Bearer ${jwt_token}` ,
      }
    }
    return fetch(URL + type, data)
      .then((response) => response.json()).then((responseData) => { 
        console.warn('out of the', responseData);
        return responseData; });
}
export const updateUser = (type,value,jwt_token) => {
     console.warn('jwt',type,jwt_token);
     console.warn("inside psotttt", value);
    let data = {
      method: 'POST',
      credentials: 'same-origin',
      mode: 'same-origin',
      body: JSON.stringify(value),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${jwt_token}` ,
      }
    }
    return fetch(URL+type, data)
      .then(response => response.json())
      .then((responseData) => {
        console.warn('out of the', responseData);
        return responseData;
      })
}
export const getLocationData = (type,jwt_token) => {
    console.warn('jwt',type,jwt_token);
    let data = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization':`Bearer ${jwt_token}` ,
      }
    }
    return fetch(type, data)
      .then((response) => response.json()).then((responseData) => { 
      console.warn('out of the', responseData);
      return responseData; });
}
export const postMethod = (type, value) => {
    console.warn("inside psot", value);
    let data = {
      method: 'POST',
      credentials: 'same-origin',
      mode: 'same-origin',
      body: JSON.stringify(value),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }
    return fetch(URL + type, data)
      .then(response => response.json())
      .then((responseData) => {
        console.warn('out of the', responseData);
        return responseData;
      });// promise
}
export const addUser = (type, value, jwt_token) => {
    console.warn("inside psot", value);
    let data = {
      method: 'POST',
      credentials: 'same-origin',
      mode: 'same-origin',
      body: JSON.stringify(value),
      headers: {
        'Content-Type': 'application/json',
        // Authorization: jwt_token ,

        'Authorization': `Bearer ${jwt_token}` ,
      }
    }
    return fetch(URL + type, data)
      .then(response => response.json())
      .then((responseData) => {
        console.warn('out of the', responseData);
        return responseData;
      });// promise
}
export const delUser = (type, jwt_token) => {
    let data = {
      method: 'POST',
      credentials: 'same-origin',
      mode: 'same-origin',
      body: '',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${jwt_token}` ,
      }
    }
    return fetch(URL + type, data)
      .then(response => response.json())
      .then((responseData) => {
        console.warn('out of the', responseData);
        return responseData;
});// promise
}