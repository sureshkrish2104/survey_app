import * as React from 'react';
import { AsyncStorage, Button, Text, TextInput, View, TouchableOpacity, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {
  createDrawerNavigator, DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import SignIn from './screens/SignIn';
import Onboarding from './screens/Onboarding';
import Welcome from './screens/Welcome';
import EditProfile from './screens/EditProfile';
import Home from './screens/Home';
import Login from './screens/Login';
import Otp from './screens/Otp';
import SurveyForm from './screens/SurveyForm';
import Splash from './components/Splash'
import UserList from './screens/UserList'
import ListUserDetail from './screens/UserDetails'
import EditUserProfile from './screens/EditUserProfile'
import Icons from 'react-native-vector-icons/MaterialIcons';
import CommonStyles from './constants/styles'
export const AuthContext = React.createContext();
const Drawer = createDrawerNavigator();
function SplashScreen() {
  return (
    <Splash />
  );
}
function CustomDrawerContent(props){
  const { signOut } = React.useContext(AuthContext);
  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <DrawerItem label={() => <Text style={{ color: 'white' }}>Logout</Text>}
        style={{ margin: 50, marginTop: 50, backgroundColor: '#0f73ee' }}
        onPress={() => signOut()}
      />
    </DrawerContentScrollView>
  );
}
const NavigationDrawerStructure = (props) => {
  //Structure for the navigatin Drawer
  const toggleDrawer = () => {
    //Props to open/close the drawer
    props.navigationProps.toggleDrawer();
  };
  return (
    <View style={{ flexDirection: 'row' }}>
      <TouchableOpacity onPress={toggleDrawer}>
        {/*Donute Button Image */}
        <Image
          source={{
            uri:
              'https://raw.githubusercontent.com/AboutReact/sampleresource/master/drawerWhite.png',
          }}
          style={{ width: 25, height: 25, marginLeft: 5 }}
        />
      </TouchableOpacity>
    </View>
  );
};
function ProfileStack({ navigation }) 
{
  return(
    <Stack.Navigator>
      <Stack.Screen name="Edit Profile" component={EditProfile}
        options=
      {
        {
          title: 'Edit Profile', //Set Header Title
          headerLeft: () => (
            <TouchableOpacity style={{ marginLeft: 10, alignItems: 'center' }} onPress={() => navigation.goBack()}>
              <Icons name='close'
                size={CommonStyles.fontSize.ICON_BACK}
                color={CommonStyles.color.COLOR_BACKGROUND_PRIMARY} />
            </TouchableOpacity>
          ),
          headerStyle: {
            backgroundColor: CommonStyles.color.COLOR_PRIMARY, //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }
    } 
  />
  </Stack.Navigator>
);
}
function HomeStack({ navigation }) {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={Home}
        options={({ navigation, route }) => ({
          title: 'Home Page',
          headerStyle: {
                backgroundColor: CommonStyles.color.COLOR_PRIMARY, //Set Header color
              },
              headerTitleStyle: {
                    fontWeight: 'bold', //Set Header text style
                  },
              headerTintColor: '#fff',
            headerLeft: () => (
            <NavigationDrawerStructure
              navigationProps={navigation}
            />
          ),
        })}
    />
      <Stack.Screen name="SurveyForm" component={SurveyForm}
        options={{
          title: 'Career Data', //Set Header Title
          headerStyle: {
            backgroundColor: CommonStyles.color.COLOR_PRIMARY, //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}/>
      <Stack.Screen name="UserDetails" component={ListUserDetail}
        options={{
          title: 'User Information', //Set Header Title
          headerStyle: {
            backgroundColor: CommonStyles.color.COLOR_PRIMARY, //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
      <Stack.Screen name="EditUserProfile" component={EditUserProfile}
       options={{
          title: 'Edit User', //Set Header Title
          headerStyle: {
            backgroundColor: CommonStyles.color.COLOR_PRIMARY, //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
        },
        }}
      >
      </Stack.Screen>
   </Stack.Navigator>
  );
}
const createHomeStack = (navigation) =>
{
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="MyDrawer"
        component={createDrawer}
      />
      </Stack.Navigator>
  );
};
const createDrawer = (navigation) => {
  return (
    <Drawer.Navigator drawerContent={props => <CustomDrawerContent {...props} />}>
      <Drawer.Screen name='Home' component={HomeStack} />
      <Drawer.Screen name='Profile' component={ProfileStack}
      />
    </Drawer.Navigator>
  );
};
function SignInScreen() {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const { signIn } = React.useContext(AuthContext);
return (
    <View>
      <TextInput
        placeholder="Username"
        value={username}
        onChangeText={setUsername}
      />
      <TextInput
        placeholder="Password"
        value={password}
        onChangeText={setPassword}
        secureTextEntry
      />
      <Button title="Sign in" onPress={() => signIn({ username, password })} />
    </View>
  );
}
const Stack = createStackNavigator();
export default function Routes({ navigation }) 
{
   const [state, dispatch] = React.useReducer(
     (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    }
);
React.useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => 
    {
      let userToken;
      try {
        userToken = await AsyncStorage.getItem('userToken');
      } catch (e) {
        // Restoring token failed
      }
      // After restoring token, we may need to validate it in production apps
      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      dispatch({ type: 'RESTORE_TOKEN', token: userToken });
    };
    bootstrapAsync();
  }, 
[]);
const authContext = React.useMemo(
    () => ({
      signIn: async (data) => {
        // In a production app, we need to send some data (usually username, password) to server and get a token
        // We will also need to handle errors if sign in failed
        // After getting token, we need to persist the token using `AsyncStorage`
        // In the example, we'll use a dummy token
        const userToken = data.token;
        const userId = data.userId;
        const userType = data.roles;
        const userName = data.userName;
        try {
          await AsyncStorage.setItem('userToken', userToken);
          await AsyncStorage.setItem('userId', userId);
          await AsyncStorage.setItem('userName', userName);
          await AsyncStorage.setItem('userType', userType);

        } catch (e) {
          // console.log(e);
        }
        // console.log('user token: ', userToken);
          dispatch({ type: 'SIGN_IN', token: userToken });
      },
      signOut: async () => {
        try {
          await AsyncStorage.removeItem('userToken');
          await AsyncStorage.removeItem('userType');
          await AsyncStorage.removeItem('userId');
          await AsyncStorage.removeItem('userName');
        }catch (e){
          console.log(e);
        }
        dispatch({ type: 'SIGN_OUT' })
      },
      signUp: async data => {
        // In a production app, we need to send user data to server and get a token
        // We will also need to handle errors if sign up failed
        // After getting token, we need to persist the token using `AsyncStorage`
        // In the example, we'll use a dummy token
        dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
      },
    }),[]
);
return(
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <Stack.Navigator>
          {state.isLoading ? (
            // We haven't finished checking for the token yet
            <Stack.Screen name="Splash" component={SplashScreen} options={{ headerShown: false }} />
          ): state.userToken == null ? (
            // No token found, user isn't signed in
            <>
              <Stack.Screen
                name="SignIn"
                component={SignIn}
                options={{
                  title: 'Sign in',
                  // When logging out, a pop animation feels intuitive
                  animationTypeForReplace: state.isSignout ? 'pop' : 'push',
                  headerShown: false
                }}
              />
            </>
          ):  
          (
                // User is signed in
                <Stack.Screen
                  name="MyDrawer"
                  options={{ headerShown: false }}
                  component={createDrawer}
                />

              )
        }     
      </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
}
